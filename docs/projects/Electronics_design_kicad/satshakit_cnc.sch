EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 12447 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4100 4600 3900 4600
Text Label 3900 4600 2    70   ~ 0
VCC
Wire Wire Line
	4100 4700 3900 4700
Text Label 3900 4700 2    70   ~ 0
VCC
Wire Wire Line
	4100 4900 3900 4900
Text Label 3900 4900 2    70   ~ 0
VCC
Wire Wire Line
	4100 5000 3900 5000
Text Label 3900 5000 2    70   ~ 0
VCC
Wire Wire Line
	2800 3500 2500 3500
Text Label 2500 3500 2    70   ~ 0
VCC
Wire Wire Line
	2300 6100 2500 6100
Text Label 2500 6100 0    70   ~ 0
VCC
Wire Wire Line
	900  3000 1200 3000
Wire Wire Line
	1200 3000 1700 3000
Wire Wire Line
	2200 3000 1700 3000
Wire Wire Line
	1700 2700 1700 3000
Wire Wire Line
	1200 2700 1200 3000
Wire Wire Line
	2200 2700 2200 3000
Connection ~ 1700 3000
Connection ~ 1200 3000
Text Label 900  3000 0    10   ~ 0
VCC
Wire Wire Line
	4100 4100 3300 4100
Wire Wire Line
	3300 4100 2600 4100
Wire Wire Line
	2600 4100 2600 3800
Connection ~ 3300 4100
Wire Wire Line
	4100 4300 3300 4300
Wire Wire Line
	3300 4300 2600 4300
Wire Wire Line
	2600 4300 2600 4600
Connection ~ 3300 4300
Wire Wire Line
	900  2100 1200 2100
Wire Wire Line
	1200 2100 1700 2100
Wire Wire Line
	1700 2100 2200 2100
Wire Wire Line
	1200 2400 1200 2100
Wire Wire Line
	1700 2400 1700 2100
Wire Wire Line
	2200 2400 2200 2100
Connection ~ 1200 2100
Connection ~ 1700 2100
Text Label 900  2100 0    10   ~ 0
GND
Wire Wire Line
	4100 5700 3900 5700
Text Label 3900 5700 2    70   ~ 0
GND
Wire Wire Line
	4100 5800 3900 5800
Text Label 3900 5800 2    70   ~ 0
GND
Wire Wire Line
	2300 3800 2100 3800
Text Label 2100 3800 2    70   ~ 0
GND
Wire Wire Line
	2300 4600 2100 4600
Text Label 2100 4600 2    70   ~ 0
GND
Wire Wire Line
	3100 2500 3200 2500
Wire Wire Line
	3200 2500 3200 2200
Connection ~ 3200 2500
Text Label 3200 2200 1    70   ~ 0
GND
Wire Wire Line
	4100 5500 3900 5500
Text Label 3900 5500 2    70   ~ 0
GND
Wire Wire Line
	2300 5200 2500 5200
Wire Wire Line
	2500 5300 2500 5200
Text Label 2500 5200 0    70   ~ 0
GND
Wire Wire Line
	2100 6600 2000 6600
Text Label 2000 6600 2    70   ~ 0
GND
Wire Wire Line
	6500 5300 8600 5300
Wire Wire Line
	6500 5400 8600 5400
Wire Wire Line
	6500 5500 8600 5500
Wire Wire Line
	6500 5600 8600 5600
Wire Wire Line
	6500 5700 8600 5700
Wire Wire Line
	6500 5800 7800 5800
Wire Wire Line
	7800 5800 8600 5800
Wire Wire Line
	7800 5800 7800 6600
Wire Wire Line
	7800 6600 2900 6600
Connection ~ 7800 5800
Wire Wire Line
	2500 5800 2500 5700
Wire Wire Line
	2600 6600 2500 6600
Wire Wire Line
	3200 2900 3100 2900
Wire Wire Line
	3200 3500 3200 2900
Wire Wire Line
	3200 2900 3500 2900
Wire Wire Line
	4100 3500 3200 3500
Wire Wire Line
	3200 2900 2700 2900
Wire Wire Line
	2700 2900 2700 1800
Connection ~ 3200 2900
Connection ~ 3200 3500
$Comp
L satshakit_cnc-eagle-import:ATMEGA48_88_168-AU MICRO1
U 1 1 B7F66DC5
P 5300 4600
F 0 "MICRO1" H 4300 5900 59  0000 L TNN
F 1 "ATMEGA328P-AU" H 4300 3200 59  0000 L BNN
F 2 "satshakit_cnc:TQFP32-08" H 5300 4600 50  0001 C CNN
F 3 "" H 5300 4600 50  0001 C CNN
	1    5300 4600
	1    0    0    -1  
$EndComp
$Comp
L satshakit_cnc-eagle-import:VCC #P+07
U 1 1 FAFE20BD
P 800 3000
F 0 "#P+07" H 800 3000 50  0001 C CNN
F 1 "VCC" V 700 2900 59  0000 L BNN
F 2 "" H 800 3000 50  0001 C CNN
F 3 "" H 800 3000 50  0001 C CNN
	1    800  3000
	0    -1   -1   0   
$EndComp
$Comp
L satshakit_cnc-eagle-import:CSM-7X-DU CRYSTAL1
U 1 1 99621192
P 3300 4200
F 0 "CRYSTAL1" H 3400 4240 59  0000 L BNN
F 1 "16Mhz" H 3400 4100 59  0000 L BNN
F 2 "satshakit_cnc:CSM-7X-DU" H 3300 4200 50  0001 C CNN
F 3 "" H 3300 4200 50  0001 C CNN
	1    3300 4200
	0    -1   -1   0   
$EndComp
$Comp
L satshakit_cnc-eagle-import:0612ZC225MAT2A C1
U 1 1 7EE2FFE3
P 2300 3800
F 0 "C1" H 2264 3909 69  0000 L BNN
F 1 "22pF" H 2225 3492 69  0000 L BNN
F 2 "satshakit_cnc:CAPC3216X178N" H 2300 3800 50  0001 C CNN
F 3 "" H 2300 3800 50  0001 C CNN
	1    2300 3800
	1    0    0    -1  
$EndComp
$Comp
L satshakit_cnc-eagle-import:0612ZC225MAT2A C2
U 1 1 1A664D53
P 2300 4600
F 0 "C2" H 2264 4709 69  0000 L BNN
F 1 "22pF" H 2225 4292 69  0000 L BNN
F 2 "satshakit_cnc:CAPC3216X178N" H 2300 4600 50  0001 C CNN
F 3 "" H 2300 4600 50  0001 C CNN
	1    2300 4600
	1    0    0    -1  
$EndComp
$Comp
L satshakit_cnc-eagle-import:GND #GND01
U 1 1 8DA2BB24
P 800 2100
F 0 "#GND01" H 800 2100 50  0001 C CNN
F 1 "GND" H 700 2000 59  0000 L BNN
F 2 "" H 800 2100 50  0001 C CNN
F 3 "" H 800 2100 50  0001 C CNN
	1    800  2100
	0    1    1    0   
$EndComp
$Comp
L satshakit_cnc-eagle-import:RESISTOR1206 R1
U 1 1 5FC72806
P 3000 3500
F 0 "R1" H 2850 3559 59  0000 L BNN
F 1 "10k" H 2850 3370 59  0000 L BNN
F 2 "satshakit_cnc:1206" H 3000 3500 50  0001 C CNN
F 3 "" H 3000 3500 50  0001 C CNN
	1    3000 3500
	1    0    0    -1  
$EndComp
$Comp
L satshakit_cnc-eagle-import:6MM_SWITCH6MM_SWITCH SWITCH1
U 1 1 BF4A4BA7
P 3100 2700
F 0 "SWITCH1" V 2850 2600 59  0000 L BNN
F 1 "6MM_SWITCH6MM_SWITCH" V 2950 2825 59  0000 L BNN
F 2 "satshakit_cnc:6MM_SWITCH" H 3100 2700 50  0001 C CNN
F 3 "" H 3100 2700 50  0001 C CNN
	1    3100 2700
	1    0    0    -1  
$EndComp
$Comp
L satshakit_cnc-eagle-import:22-23-2021 POWER1
U 1 1 AF873C25
P 2200 6100
F 0 "POWER1" H 2300 6070 51  0000 L BNN
F 1 "22-23-2021" H 2170 6155 59  0001 L BNN
F 2 "satshakit_cnc:22-23-2021" H 2200 6100 50  0001 C CNN
F 3 "" H 2200 6100 50  0001 C CNN
	1    2200 6100
	-1   0    0    1   
$EndComp
$Comp
L satshakit_cnc-eagle-import:22-23-2021 POWER1
U 2 1 AF873C29
P 2200 5200
F 0 "POWER1" H 2300 5170 51  0000 L BNN
F 1 "22-23-2021" H 2170 5255 59  0001 L BNN
F 2 "satshakit_cnc:22-23-2021" H 2200 5200 50  0001 C CNN
F 3 "" H 2200 5200 50  0001 C CNN
	2    2200 5200
	-1   0    0    1   
$EndComp
$Comp
L satshakit_cnc-eagle-import:LED1206 LED_13
U 1 1 D663D747
P 2700 6600
F 0 "LED_13" V 2840 6520 59  0000 L BNN
F 1 "YELLOW" V 2925 6520 59  0000 L BNN
F 2 "satshakit_cnc:1206" H 2700 6600 50  0001 C CNN
F 3 "" H 2700 6600 50  0001 C CNN
	1    2700 6600
	0    1    1    0   
$EndComp
$Comp
L satshakit_cnc-eagle-import:LED1206 LED_GREEN1
U 1 1 741F3F92
P 2500 5900
F 0 "LED_GREEN1" V 2640 5820 59  0000 L BNN
F 1 "LED1206" V 2725 5820 59  0000 L BNN
F 2 "satshakit_cnc:1206" H 2500 5900 50  0001 C CNN
F 3 "" H 2500 5900 50  0001 C CNN
	1    2500 5900
	-1   0    0    1   
$EndComp
$Comp
L satshakit_cnc-eagle-import:RESISTOR1206 R2
U 1 1 6A2B927F
P 2500 5500
F 0 "R2" H 2350 5559 59  0000 L BNN
F 1 "499" H 2350 5370 59  0000 L BNN
F 2 "satshakit_cnc:1206" H 2500 5500 50  0001 C CNN
F 3 "" H 2500 5500 50  0001 C CNN
	1    2500 5500
	0    -1   -1   0   
$EndComp
$Comp
L satshakit_cnc-eagle-import:RESISTOR1206 R3
U 1 1 4F7F9F8C
P 2300 6600
F 0 "R3" H 2150 6659 59  0000 L BNN
F 1 "499" H 2150 6470 59  0000 L BNN
F 2 "satshakit_cnc:1206" H 2300 6600 50  0001 C CNN
F 3 "" H 2300 6600 50  0001 C CNN
	1    2300 6600
	1    0    0    -1  
$EndComp
$Comp
L satshakit_cnc-eagle-import:CAP1206 C3
U 1 1 C3EA450F
P 2200 2500
F 0 "C3" H 2260 2615 59  0000 L BNN
F 1 "10uF" H 2260 2415 59  0000 L BNN
F 2 "satshakit_cnc:1206" H 2200 2500 50  0001 C CNN
F 3 "" H 2200 2500 50  0001 C CNN
	1    2200 2500
	-1   0    0    1   
$EndComp
$Comp
L satshakit_cnc-eagle-import:CAP1206 C4
U 1 1 674C1B16
P 1700 2500
F 0 "C4" H 1760 2615 59  0000 L BNN
F 1 "1uF" H 1760 2415 59  0000 L BNN
F 2 "satshakit_cnc:1206" H 1700 2500 50  0001 C CNN
F 3 "" H 1700 2500 50  0001 C CNN
	1    1700 2500
	-1   0    0    1   
$EndComp
$Comp
L satshakit_cnc-eagle-import:CAP1206 C5
U 1 1 8E04CE4C
P 1200 2500
F 0 "C5" H 1260 2615 59  0000 L BNN
F 1 "100nF" H 1260 2415 59  0000 L BNN
F 2 "satshakit_cnc:1206" H 1200 2500 50  0001 C CNN
F 3 "" H 1200 2500 50  0001 C CNN
	1    1200 2500
	-1   0    0    1   
$EndComp
$Comp
L satshakit_cnc-eagle-import:CAP1206 C6
U 1 1 21A596BE
P 3700 2900
F 0 "C6" H 3760 3015 59  0000 L BNN
F 1 "100nF" H 3760 2815 59  0000 L BNN
F 2 "satshakit_cnc:1206" H 3700 2900 50  0001 C CNN
F 3 "" H 3700 2900 50  0001 C CNN
	1    3700 2900
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 TDSsensor1
U 1 1 619D55BA
P 7150 3250
F 0 "TDSsensor1" H 7230 3292 50  0000 L CNN
F 1 "Conn_01x03" H 7230 3201 50  0000 L CNN
F 2 "eagle_pin:03P" H 7150 3250 50  0001 C CNN
F 3 "~" H 7150 3250 50  0001 C CNN
	1    7150 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 3250 6750 3250
Wire Wire Line
	6950 3150 6750 3150
Wire Wire Line
	6950 3350 6750 3350
Text GLabel 6750 3150 0    50   Input ~ 0
VCC
Wire Wire Line
	6500 3500 6850 3500
Text GLabel 6850 3500 2    50   Input ~ 0
ADC0
$Comp
L Connector_Generic:Conn_01x03 Temperature_sensor1
U 1 1 619E721A
P 8600 3250
F 0 "Temperature_sensor1" H 8680 3292 50  0000 L CNN
F 1 "Conn_01x03" H 8680 3201 50  0000 L CNN
F 2 "eagle_pin:03P" H 8600 3250 50  0001 C CNN
F 3 "~" H 8600 3250 50  0001 C CNN
	1    8600 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 3250 8100 3250
Wire Wire Line
	8400 3150 8100 3150
Wire Wire Line
	8400 3350 8100 3350
Text GLabel 8100 3150 0    50   Input ~ 0
VCC
Text GLabel 8100 3350 0    50   Input ~ 0
ADC1
Text GLabel 8100 3250 0    50   Input ~ 0
GND
Wire Wire Line
	6500 3600 6850 3600
Text GLabel 6850 3600 2    50   Input ~ 0
ADC1
$Comp
L Connector_Generic:Conn_01x03 pHsensor1
U 1 1 61A05055
P 10200 3250
F 0 "pHsensor1" H 10280 3292 50  0000 L CNN
F 1 "Conn_01x03" H 10280 3201 50  0000 L CNN
F 2 "eagle_pin:03P" H 10200 3250 50  0001 C CNN
F 3 "~" H 10200 3250 50  0001 C CNN
	1    10200 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 3250 9750 3250
Wire Wire Line
	10000 3150 9750 3150
Wire Wire Line
	10000 3350 9750 3350
Text GLabel 9750 3150 0    50   Input ~ 0
VCC
Text GLabel 9750 3350 0    50   Input ~ 0
ADC2
Text GLabel 9750 3250 0    50   Input ~ 0
GND
Wire Wire Line
	6500 3700 6850 3700
Text GLabel 6850 3700 2    50   Input ~ 0
ADC2
$Comp
L Connector_Generic:Conn_01x03 Photoresistor1
U 1 1 61A1D925
P 11450 3250
F 0 "Photoresistor1" H 11530 3292 50  0000 L CNN
F 1 "Conn_01x03" H 11530 3201 50  0000 L CNN
F 2 "eagle_pin:03P" H 11450 3250 50  0001 C CNN
F 3 "~" H 11450 3250 50  0001 C CNN
	1    11450 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	11250 3250 11050 3250
Wire Wire Line
	11250 3150 11050 3150
Wire Wire Line
	11250 3350 11050 3350
Wire Wire Line
	6500 3800 6850 3800
Text GLabel 11050 3150 0    50   Input ~ 0
VCC
Text GLabel 11050 3350 0    50   Input ~ 0
ADC3
Text GLabel 11050 3250 0    50   Input ~ 0
GND
Text GLabel 6850 3800 2    50   Input ~ 0
ADC3
$Comp
L Connector_Generic:Conn_01x04 LCD_driver1
U 1 1 61A49701
P 8300 3900
F 0 "LCD_driver1" H 8380 3892 50  0000 L CNN
F 1 "Conn_01x04" H 8380 3801 50  0000 L CNN
F 2 "eagle_pin:04P" H 8300 3900 50  0001 C CNN
F 3 "~" H 8300 3900 50  0001 C CNN
	1    8300 3900
	-1   0    0    1   
$EndComp
Text GLabel 8750 3700 2    50   Input ~ 0
VCC
Text GLabel 8750 3900 2    50   Input ~ 0
SDA
Text GLabel 8750 4000 2    50   Input ~ 0
SCL
Wire Wire Line
	6500 3900 6850 3900
Wire Wire Line
	6500 4000 6850 4000
Text GLabel 6850 3900 2    50   Input ~ 0
SDA
Text GLabel 6850 4000 2    50   Input ~ 0
SCL
$Comp
L Connector_Generic:Conn_01x04 bluetoothtransceiver1
U 1 1 61A72E52
P 9600 4000
F 0 "bluetoothtransceiver1" H 9518 3575 50  0000 C CNN
F 1 "Conn_01x04" H 9518 3666 50  0000 C CNN
F 2 "eagle_pin:04P" H 9600 4000 50  0001 C CNN
F 3 "~" H 9600 4000 50  0001 C CNN
	1    9600 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	8500 3800 8750 3800
Wire Wire Line
	8500 3900 8750 3900
Wire Wire Line
	8500 4000 8750 4000
Wire Wire Line
	9800 3800 10050 3800
Wire Wire Line
	9800 3900 10050 3900
Wire Wire Line
	9800 4000 10050 4000
Wire Wire Line
	9800 4100 10050 4100
Text GLabel 10050 3800 2    50   Input ~ 0
VCC
Text GLabel 10050 3900 2    50   Input ~ 0
GND
Text GLabel 10050 4000 2    50   Input ~ 0
TXD
Text GLabel 10050 4100 2    50   Input ~ 0
RXD
Wire Wire Line
	6500 4400 6850 4400
Wire Wire Line
	6500 4500 6850 4500
Text GLabel 6850 4500 2    50   Input ~ 0
TXD
Text GLabel 6850 4400 2    50   Input ~ 0
RXD
Wire Wire Line
	3950 1800 3950 1600
Wire Wire Line
	2700 1800 3950 1800
$Comp
L Connector_Generic:Conn_01x03 J2
U 1 1 61AFBE1C
P 10150 5350
F 0 "J2" H 10230 5392 50  0000 L CNN
F 1 "Conn_01x03" H 10230 5301 50  0000 L CNN
F 2 "eagle_pin:03P" H 10150 5350 50  0001 C CNN
F 3 "~" H 10150 5350 50  0001 C CNN
	1    10150 5350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J5
U 1 1 61AFD47D
P 11300 5300
F 0 "J5" H 11380 5342 50  0000 L CNN
F 1 "Conn_01x03" H 11380 5251 50  0000 L CNN
F 2 "eagle_pin:03P" H 11300 5300 50  0001 C CNN
F 3 "~" H 11300 5300 50  0001 C CNN
	1    11300 5300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J3
U 1 1 61AFFB2E
P 10150 6750
F 0 "J3" H 10230 6792 50  0000 L CNN
F 1 "Conn_01x03" H 10230 6701 50  0000 L CNN
F 2 "eagle_pin:03P" H 10150 6750 50  0001 C CNN
F 3 "~" H 10150 6750 50  0001 C CNN
	1    10150 6750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J4
U 1 1 61B01DE5
P 10250 5950
F 0 "J4" H 10330 5992 50  0000 L CNN
F 1 "Conn_01x03" H 10330 5901 50  0000 L CNN
F 2 "eagle_pin:03P" H 10250 5950 50  0001 C CNN
F 3 "~" H 10250 5950 50  0001 C CNN
	1    10250 5950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J6
U 1 1 61B04000
P 11450 5950
F 0 "J6" H 11530 5992 50  0000 L CNN
F 1 "Conn_01x03" H 11530 5901 50  0000 L CNN
F 2 "eagle_pin:03P" H 11450 5950 50  0001 C CNN
F 3 "~" H 11450 5950 50  0001 C CNN
	1    11450 5950
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J7
U 1 1 61B06A0F
P 11500 6650
F 0 "J7" H 11580 6692 50  0000 L CNN
F 1 "Conn_01x03" H 11580 6601 50  0000 L CNN
F 2 "eagle_pin:03P" H 11500 6650 50  0001 C CNN
F 3 "~" H 11500 6650 50  0001 C CNN
	1    11500 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 5250 9700 5250
Wire Wire Line
	9950 5350 9700 5350
Wire Wire Line
	9950 5450 9700 5450
Wire Wire Line
	11100 5200 10950 5200
Wire Wire Line
	11100 5400 10950 5400
Wire Wire Line
	11100 5300 10950 5300
Wire Wire Line
	9950 6650 9800 6650
Wire Wire Line
	9950 6750 9800 6750
Wire Wire Line
	9950 6850 9800 6850
Wire Wire Line
	11300 6550 11200 6550
Wire Wire Line
	11300 6650 11200 6650
Wire Wire Line
	11300 6750 11200 6750
Wire Wire Line
	11250 5850 11150 5850
Wire Wire Line
	11250 5950 11150 5950
Wire Wire Line
	11250 6050 11150 6050
Wire Wire Line
	10050 5850 9900 5850
Wire Wire Line
	10050 5950 9900 5950
Wire Wire Line
	10050 6050 9900 6050
Text GLabel 10950 5400 0    50   Input ~ 0
VCC
Text GLabel 9800 6650 0    50   Input ~ 0
ICP1
Text GLabel 9700 5350 0    50   Input ~ 0
GND
Text GLabel 10950 5200 0    50   Input ~ 0
VCC
Text GLabel 9800 6750 0    50   Input ~ 0
OC1A
Text GLabel 9700 5450 0    50   Input ~ 0
GND
Text GLabel 10950 5300 0    50   Input ~ 0
VCC
Text GLabel 9800 6850 0    50   Input ~ 0
SS
Text GLabel 9900 5950 0    50   Input ~ 0
GND
Text GLabel 11150 5850 0    50   Input ~ 0
VCC
Text GLabel 11200 6550 0    50   Input ~ 0
MOSI
Text GLabel 9700 5250 0    50   Input ~ 0
GND
Text GLabel 11150 5950 0    50   Input ~ 0
VCC
Text GLabel 11200 6650 0    50   Input ~ 0
MISO
Text GLabel 9900 5850 0    50   Input ~ 0
GND
Text GLabel 11150 6050 0    50   Input ~ 0
VCC
Text GLabel 11200 6750 0    50   Input ~ 0
SCK
Text GLabel 9900 6050 0    50   Input ~ 0
GND
Text GLabel 8600 5300 2    50   Input ~ 0
ICP1
Text GLabel 8600 5400 2    50   Input ~ 0
OC1A
Text GLabel 8600 5500 2    50   Input ~ 0
SS
Text GLabel 8600 5600 2    50   Input ~ 0
MOSI
Text GLabel 8600 5700 2    50   Input ~ 0
MISO
Text GLabel 8600 5800 2    50   Input ~ 0
SCK
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 61B5EAA7
P 3950 1400
F 0 "J1" V 3914 1212 50  0000 R CNN
F 1 "Conn_01x02" V 3823 1212 50  0000 R CNN
F 2 "eagle_pin:02P" H 3950 1400 50  0001 C CNN
F 3 "~" H 3950 1400 50  0001 C CNN
	1    3950 1400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4050 1600 4050 2900
Wire Wire Line
	4050 2900 3800 2900
Text GLabel 6750 3250 0    50   Input ~ 0
GND
Text GLabel 6750 3350 0    50   Input ~ 0
ADC0
Wire Wire Line
	8500 3700 8750 3700
Text GLabel 8750 3800 2    50   Input ~ 0
GND
$EndSCHEMATC
