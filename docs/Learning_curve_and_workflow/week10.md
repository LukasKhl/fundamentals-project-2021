# 10. Output devices

This week I am supposed to get some sort of output from my PCB.
I decided to get my data from my sensors displayed on a LCD display and transmitted to my phone via a bluetooth transceiver (brands and models see week 7).
Before that I wanted to install two more sensors, that I would need for my final project.

One being a pH-meter and one being a phototransistor.

## Phototransistor (light intensity)

First I found myself a diagram of how to connect the phototransistor to the board (upper part of the breadboard).

![picture](../images/week_10/Screenshot_1.png)

The respective pins on my board for GND, VCC and data:
![picture](../images/week_10/Screenshot_2.png)

For the arduino IDE either A3 or 17:
![picture](../images/week_10/Screenshot_3.png)

The phototransistor is a device with two bare wires sticking out, of which the shorter one is for VCC and the longer for GND. This might actually be confusing at first, since in an LED it is vice versa.

![picture](../images/week_10/Screenshot_4.png)

I plugged the code for the phototransistor in from the web. I needed to define the pin on the arduino for it first (17) and then defined it as an input in the void setup.
![picture](../images/week_10/Screenshot_5.png)

The type of input that it delivers is described as a float datatype (decimal number). Volt is defined before in the void loop as the variable describing the input of the sensor and then in this functio, it is declared as the integer adPin. Integer because it is the pin 17 I believe...
The brackets {} are there because they frame the content of the function. With the command analogRead we read the input from pin 17 and since we get a value that is translated from the voltage comming in from the data pin, we need to tanslte it back to voltage. Means: It needs to be multiplied by 5V and divided by 1024.
![picture](../images/week_10/Screenshot_6.png)

## pH-meter
Daigram for connection to arduino:
![picture](../images/week_10/Screenshot_7.png)

In my cas: Data pin left, GNG in the middle and VCC on the right:

![picture](../images/week_10/Screenshot_8.png)

For the declaration in the arduino IDE, it is possible to either use A2 or 16:

![picture](../images/week_10/Screenshot_9.png)

I declared it as 16. Const int in this case means, that it is a variable to be read only and that it also serves as a reference point. That means it is the reference point, from which the pH in-/decreases in this case:

![picture](../images/week_10/Screenshot_10.png)

I included that on the bottom of my existing void loop:

![picture](../images/week_10/Screenshot_11.png)

The pH value read is outside the possible range, but that is because I did not calibrate the pH meter beforehand:

![picture](../images/week_10/Screenshot_12.png)

The source code and some tips, I took from a website called etinker:

![picture](../images/week_10/Screenshot_13.png)

## The LCD display
To connect the LCD display, I first checked for a diagram, using an I2C transmitter in combination:
![picture](../images/week_10/Screenshot_14.jpg)

The respective pins on my PCB:

![picture](../images/week_10/Screenshot_15.png)

Since I had two devices with pins each, I chose to use the breadboard to connect them:

![picture](../images/week_10/Screenshot_16.JPG)

(Got some cable-chaos going on here...)

On the web I found the following code. It had me include the wire library and the LiquidCrystal_I2C library.
It is important as always to indicate the address of communication. With the I2C driver the adress depends on that driver and not as before on the pins on the board (SDA and SCL here). The adress is 0x27 as can be seen in line 4.

![picture](../images/week_10/Screenshot_16.5.png)

[Click here for the source](https://create.arduino.cc/projecthub/Arnov_Sharma_makes/lcd-i2c-tutorial-664e5a)

Here you can see the code implemented in my sensor code.
![picture](../images/week_10/Screenshot_17.png)

There are several Liquid_Crystal libraries. I researched a bit and I needed the one by Marc Schwarz.
![picture](../images/week_10/Screenshot_18.png)

Here I copied the code and transferred all the printouts from the serielle monitor to the LCD, so I would have the same readings there. I shortened a few strings, since each line on the LCD has a maximum output of 16 characters.
![picture](../images/week_10/Screenshot_18.5.png)

Now I had the problem that nothing was shown on the LCD after uploading the code. 
![picture](../images/week_10/Screenshot_19.jpg)

First I cheched, if the adress 27x0 was correct. For that I found a code online, that checks what the correct adress of a connected I2C device would be.

![picture](../images/week_10/Screenshot_20.png)
[Click here for the source](https://playground.arduino.cc/Main/I2cScanner/)

It appeared, that 0x27 was actually correct, so I checked the website of the manufacturer of the I2C device (I2C IIC Adapter):

https://www.az-delivery.de/products/serielle-schnittstelle

It is equipped with a datasheet for the device, where I found information on how to opperate the LCD:

![picture](../images/week_10/Screenshot_21.png)

Turned out, the blue framed screw is there to adjust the contrast of the board, which makes it possible to see the letters.

Then it started working:

![picture](../images/week_10/Screenshot_22.JPG)

But not entirely to my happiness, since when the screen switched not all the text from the previous lines is deleted:

![picture](../images/week_10/Screenshot_23.gif)

It kept showing Volt after the pH value, what is obviously not correct (and yes the pH-meter is still not calibrated yet).

So I went back to the code and switched a few things up:

```
 lcd.init();                      // initialize the lcd 

  // Print a message to the LCD.
  lcd.backlight();
  lcd.setCursor(1,0);
  lcd.print("Light: " + String(volt(17)) + "V");
  lcd.setCursor(1,1);
  lcd.print("TDS:" + String(tdsValue, 0) + "ppm");
  delay(5000);
  lcd.clear();
  lcd.backlight();
  lcd.setCursor(1,0);
  lcd.print("pH: " + String(phValue));
  lcd.setCursor(1,1);
  lcd.print("Temp:" + String(sensors.getTempCByIndex(0)) + "degC");
  delay(5000);
  lcd.clear();
```


The main thing I changed, is to use lcd.clear() after every delay, making sure not to have any text from the previous part of the loop left on the display:

![picture](../images/week_10/Screenshot_24.gif)

## Bluetooth transceiver

RX&rarr; Receive
  
TX&rarr; Transmit

Software Serial example: https://www.arduino.cc/en/Tutorial/LibraryExamples/SoftwareSerialExample

Since the bluetooth transceiver I have is not compatible with my phone, I will see that I can order a new one, to adjust and use it in my final project.



