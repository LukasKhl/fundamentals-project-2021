# 3. Computer aided Design and Lasercut

This week the task was to design a prototype of something self-defined in fusion and export it as a dxf to be then able to use the lasercutter to materialize it.

Download the file [here](https://gitlab.com/LukasKhl/fundamentals-project-2021/-/raw/main/docs/projects/CAD/laser_cut_box_v1.f3d)

## Design in Fusion 360

I wanted to make the box that I designed last week with the lasercutter. In order to connect the sides, which are seperate when cutting them with a laser, I used a finger pocket system, to be able to stack them.

In fusion that meant to first do sketches with fixed parameters, so the design would be parametric.

![step_0](../images/week03/step_0.JPG)

After that I drew a first construction rectangle to have an outline for the finger/pocket bottom piece. Then I drew it using finger/pocket for the length of the inner and outer borders parallel to the rectangle, using - or + kerf respectably to compensate the slack of the lasercutter beam, which I estimated to be 0.1mm. For the encarving of the pockets I used the parameter thickness (5mm), in order to fit the material with a thickness of 5mm. After having made a few "steps" of fingers and pockets, I replicated it using rectangular pattern.

![step_1](../images/week03/step_1.png)
![step_2](../images/week03/step_2.png)

I used spacing and gave it the respective direction (+/-20cm respectively on the y or x axis, because one finger + one pocket shall be 20cm inlength) and then gave it a numer of how many times the rectangular pattern was to be repeated (until it filled out the length or width).

Sometimes corners to be adjusted to the length of the box or in order to fit with another side of the box, had to be adjusted in size. That was always a fraction of the finger/pocket length (10mm). Usually that would be 1/2*finger+kerf (or -kerf when it was pocket).

![step_3](../images/week03/step_3.JPG)

After I was done making the 6 sketches, I extruded them, right clicked them in the overview task bar and transformed them into components, so I could join them (before I rotated them in the right position to simplify the process).

![step_4](../images/week03/step_4.png)

For the hole in the box, where I later on want to put a transparent material, I offset a rectangular shape in a sketch right over the top piece of the box and rounded the edges. Then I negatively extruded it (-thickness=5mm), to create a hole in the component.

![step_5](../images/week03/step_5.png)
![step_6](../images/week03/step_6.png)

To later cut the materials, I had to export them as dxf files. Right clicking the sketches and clicking save_as_dxf did the job.

## How to set up the Laser cutter 

For setting up the laser, we need to unlock the movable mechanics of the laser and put the laser to the upper left corner of the material (ideally). Set the distance with the spring in the device to ensure the correct distance from the material (to cut through properly) and then press the play button to lock the settings.
Now the files need to be set for the print. For that, we open them with a software that is compatible with the zing laser, put all the components in the right spot (digitally), so the max can be cut with minimum material losses.
It is important to then adapt the settings right. If you are doing an engraving, choose raster for the job, if it is cutting, choose vector. Order the jobs in such way that when raster and vector jobs need to be done in the same job, raster jobs are done first, because the cutting of the vector job might offset the material vertically. Three setting for the cut to be considered are speed, frequency and power. Thicker material requires higher power and lesser speed. When engraving one might want to lower speed but also frequency as higher frequnecy causes more heat and potential burning. If unsure, which settings to use, there is a booklet to be found by the laser cutter (in the workshoop in Kamp Lintfort), which suggests settings for different materials with different thicknesses.
When all settings are clear, set the point where the lasercut shall start from (digitally) and then press print (digitally and analog).

In the end I had to assemble my pieces into box shape:

![step_7](../images/week03/step_7.jpg)


