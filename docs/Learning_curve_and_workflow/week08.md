# 8. Embedded programming

This weeks the goal is to use and implement code for our selfmade PCB. Since I want to use my sensors together with the board, I will try to use one of them as an example for this weeks task and perhaps the LCD as an outputdevice to show the data collected with my sensor.

## The basics

Programming is very new to me, so I want to go step by step in order to gain some good foundational understanding before getting started. In order to do so, first I plan to go through the lecture slides and the recording of the lecture again. I want to make sure, that I know roughly how to help myself here, so I want to first pile up all the tips I possible can from the lecture.

### Knowledge taken from the lecture
*pictures I do not assign an origin to are taken from material provided by FabLab supervisor Daniele Ingrassia.

### Data types, bytes and bits

Here is a table from the lecture, that to my understanding describes the different data types that can be assigned to a variable.
![step_1](../images/week08/Screenshot_1.png)

The table shows first how much memory a variable of a certain data type would consume or in other words which size it has.This is measured in bytes. Bytes are based on bits, precisely on arrays of 8 bits (e.g. [0,0,0,0,1,1,0,1]). What are bits then you ask? Well the different constellations of zeros and ones you can have in an 8-bit array, 256 precisely, are the basic code in computer science for numbers, letter and signs (with the combination of several arrays (combinations of letters, numbers and signs) you can then code/command other stuff). In the physical realm these zeros and ones have different possible sources, which is fundamentally based on electricity. Simplified zero and one are determined by voltage or electrical current impulses.

&rarr; Voltage/current= measurable &rarr; 1

&rarr; Voltage/current= not measurable &rarr; 0

(roughly... in reality with microcontrollers it can be 1.5V - 5V: 1;
-0.5 - 1.5:0)

I found the following list of what it could be precisely:

* Switch position in an electrical circuit
* the charge state of a capacitor,
* the voltage level at an electronic component
* the circuit state of a semiconductor device,
* the electrical charge state of a field effect transistor,
* the magnetization state of a magnetizable layer (electromagnet).

My guess is, that it depends on the type of device you use, when it comes to which exact method to use for the voltage or current pulse.

Byte is for example a good data type for the pins. It can be a number from 0 to 255 and takes only one byte in memory.

But now back to the definition of the data types:

Data types are mostly describing ranges of numbers, some rather small ranges and some rather large ranges. Most without decimals and two with (float and double). A few also describe other properties like verification (boolean) or ASCII codes, which include basically the alphabet and a bunch of signs, that one probably knows from the sign list of their office program (char, unsigned char, byte and uint8_t).

Here are examples for ASCII codes:

 ![step_2](../images/week08/screenshot_2.png)
 
 Picture-source: [Wikipedia](https://de.wikipedia.org/wiki/Datei:Dosasciitable_CP437.png)

 

### Declaring datatypes at the beginning of writing your code
With these datatypes we can declare variables we are going to use throughout the coding process.

Example from the arduino IDE:
```
byte counter=0; //counter as a logical name for what is supposed to hapen with this variable...it shall be used to count a process; byte is the datatyoe assigned.

boolean buttonState ; //the variable is called buttonState since a button can be pushed down or not, translates to true or false (boolean)
```
&rarr;  choosing the variable names with a logical connection to what is happening with them in the code is considered good practice.

### The Syntax or in other words the commands your programming language uses

Syntax to my understanding is the collection of code-commands with which you communicate with your device respective of the programming language you use (C++, python, java,...).

We were given two slides of syntax in the lecture:

![step_3](../images/week08/Screenshot_3.png)

These I would argue are quite straight forward if you compare them to what you learned in high school math.
Simply the modulo (%) was new to me. It accounts how many "full number steps" left after a division of two whole-numbers. 7%5 &rarr; 7=5+1+1 &rarr; 2 number steps.

![step_4](../images/week08/Screenshot_4.png)

The syntax in the second picture I do not know through related fields like the first, but the explanations in the picture I think are selfexplaining enough.
Most important here is to use the ; after every finished line in the code and something I witnessed a lot is the use of #define... , which is used to allocate names to the pins of your board.


The next slide in the lecture describes "control structures", which are basically about the parameter time as in how often/how long/ if at all a section of the code is acted upon. The words here basically mean what they mean in a conversation as well (if, while,...):

![step_5](../images/week08/Screenshot_5.png)

### Functions

The next slide describes how a function in code is structured.

Terms to clarify:

Serial &rarr; basically stands for "one event at a time", for the communication of the mcu with another device this could mean one information fprwarded/received at a time. It is also connected to the baud rate, the bits transferred per second, so it determines how fast information can be exchanged.

void &rarr; void is a return type and indicates that a function does not return a value.

![step_6](../images/week08/Screenshot_6.png)

The next slide deals with functions. Functions are blocks of code, that are used when they are called. Data/parameters can be passed into a function. Functions are important since they allow code blocks to be reused.

Example for a function:
![step_7](../images/week08/Screenshot_7.png)

 X and y are being called in the function and must have been defined before. Here it is clarified that they are integer-datatype. The value to gain from this function, namely "result" is defined in the function content as an integer as well. To make use of "result", the command 

 ```
 return result;
 ```
 is used.

In this example a function is created in the end of the code (int myMultiplyFunction...) and called in the middle (k= myMultiplyFunction).

![step_8](../images/week08/Screenshot_8.png)

### Digital inputs and outputs

Digital inputs' and outputs' allocation need to be defined within the code. For example:

```
int ledPin 13;
```
defines that at pin 13 of a mcu an Led is connected to.
If the Led is an input or output also needs to be defined:

```
pinMode(ledPin, OUTPUT;
```
Be careful to write caps when required. The arduino IDE is caps sensitive.

digitalWrite and digitalRead define if an output is commanded or the input reading of a device is commanded.

This code (first 13 lines) describes a program where the push/non-push state of a button is connected to an Led being on or off.

The rest of the lines describe another programm where an led blink (on 1s, off 1s):

![step_9](../images/week08/Screenshot_9.png)

The next example shows, that the commands are slightly different when dealing with pins meant for analog devices:

![step_10](../images/week08/Screenshot_10.png)

The following examples show for loops and how to delay in- and outputs defined times:

![step_11](../images/week08/Screenshot_11.png)

### Serial
Serial as mentioned here before is about transferring data, one bit at a time with a certain speed (baud rate, e.g. 9600 bauds or bps(bits per second)). Sometimes, depending to which device they are hooked up to, some pins need their own attributed baud rate.

![step_12](../images/week08/Screenshot_12.png)

.begin(57600) or .begin(4800) for example determinie the baud rate needed at certain points in the code.

### I2C
I2C stands for inter-integrated circuit and is an onboard communication protocoll. This protcoll foresees a primary device (alternatively: controller/intiator) to request a secondary device (alternatively: agent/follower) to start a transition of information between the two. The two wires are: The serial data wire (SDA) and the serial clock wire (SCL). One primary device can be hooked to several secondary devices. Prior primary and secondary device were called master and slave device.

![step_13](../images/week08/Screenshot_13.png)

A code for an intended I2C communication could look like this:

![step_14](../images/week08/Screenshot_14.png)

To know how to define your pins in your respective project, it is always recommendable to look out your respective mcu's pinout scheme:

![step_15](../images/week08/Screenshot_15.png)

Good news is, that for most devices, that can be connected with an arduino, libraries with source code exist:

![step_16](../images/week08/Screenshot_16.png)

The arduino IDE has its own programming language, which is based on C++. C++ can though also be used in the arduino IDE and will speed up the programm since there is no intermediary step of translation.

![step_17](../images/week08/Screenshot_17.png)

Example code from the lecture (LED blinking predefined number of times):

```

// this is some playin around
#define BUTTON_PIN 8
#define LED_PIN 13
byte ledPin =13; //byte "costs" less memory than int, byte has a max number of happening (something around 250)
//you couls also #define LED_PIN 13 if you are sure it shall not change throughout the program
byte counter=0; //counter because it's an understandable word in the sense of the functio chosen as a variable
byte iterations=5; //Iterations=Durchläufe as a variable
boolean buttonState ;


void setup() { //anfänglicher code, der nur einmal läuft

 pinMode(ledPin, OUTPUT); //defines the led as an output
 pinMode(buttonState, INPUT_PULLUP);
 Serial.begin(9600); //begin implies that we start communicating with the electronic device (the arduino) -->      at a rete of 9600 bits/second
}

void loop() { //code which repeats as long as there is data storage available on the board

 buttonState= digitalRead(BUTTON_PIN);
if (buttonState){

 digitalWrite(LED_PIN, HIGH);

 }else{

  digitalWrite(LED_PIN,LOW);

 }
 
 for (;counter<iterations;counter++){
 blinking();
 Serial.print("Blink no"); //is presented in the serial monitor, which can be found on the top right
 Serial.println(counter+1); //"counts" the blinkings by adding them, also to be seen in the serial monitor
}
}
void blinking(){
 digitalWrite(ledPin, HIGH); //turns the led on
 delay(1000); //1000ms = 1s delay as in duration
 digitalWrite(ledPin, LOW); // LOW==off
 delay(1000); 
}
```

### Serial monitor and baud rate 

To get a data output on your computerdisply, in case you do not have an output device on your computer for it, you can use the serial monitor. Within you need to use the right baud rate, to get the data displayed properly.

![step_18](../images/week08/Screenshot_18.png)

### Important note

When connecting devices to the PCB, it is always recommendable to check if they need to be connected with an electronic component like a resistor (pull up or pull down).

## Programming a temperature sensor

For my temperature sensor, I found a coding template, that I slightly changed to make it work to my needs.
My sensor is called EXP-R15-019 by Adafruit. I figured out that it belongs to a group of temperature sensors with the name DS18B20.
Searching on the web, I found a code template.
Here are my changes to it.

From pin 2 to 24 (looking on the pinout sketch of the atmega328P-AU, I first believed this was the right name of the pin. I had to change it later...)

![step_19](../images/week08/Screenshot_19.png)

I changed waiting time between the output-reading from 1s to 5s (1000ms to 5000ms).

![step_20](../images/week08/Screenshot_20.png)

Two libraries needed to be installed for the code to work. Their names were to be taken from the beginning of the code:

![step_21](../images/week08/Screenshot_21.png)

![step_22](../images/week08/Screenshot_22.png)

This is the entire code:

![step_23](../images/week08/Screenshot_23.png)

To connect the board correctly to the board, I checked a picture I found online:

![step_24](../images/week08/Screenshot_24.png)

GND and VCC (GND is blue in case of this sensor, normally black). The data cable here goes to pin 2, which is different in my selfmade board. To check where it needed to go, I took a look at my layout.
And found this tip from the manufacturer:

![step_25](../images/week08/Screenshot_25.png)

After verifying and uploading the code in the arduino IDE, I made sure the baud rate was correct, when trying to read the data in the serial monitor:

![step_26](../images/week08/Screenshot_26.png)

I got readings, but they were a little too low... If my room was that cold, I don't think I would be writing this.

![step_27](../images/week08/Screenshot_27.png)

So that meant one of two things. Either the code was wrong (but it was provided like this by the supplier) or something in my pinout connections was not right.
Eventually a colleague gave me a hint:
He told me to look close again at the pinout sketch:

![step_28](../images/week07/Screenshot_73.png)

The pin number is 24 yes, but looking at the legend for an arduino that transfers to pin A0 or 15.

So Ichanged that in the code:

![step_28](../images/week08/Screenshot_28.png)

Resulting in more realistic data:

![step_29](../images/week08/Screenshot_29.png)

For a video showing the program working, check next weeks progress.

Download the code to the sensor [here](https://gitlab.com/LukasKhl/fundamentals-project-2021/-/raw/main/docs/projects/Code/sketch_sensor.ino?inline=false).