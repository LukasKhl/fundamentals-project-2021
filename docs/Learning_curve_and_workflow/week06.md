# 6. 3D Design and printing

This week we learned how to use a 3D printer and some of the theory behind it.

## Design process

As in the previous weeks, I used Fusion360 to design my prototype for this assignment.
I skip describing a few steps at this point, since I have done similar things in the previous weeks.
First I sketched two rectangles and circles and extruded them.
The cylinders made from the circles I turned over on their sides. Then I sketched circles onto their surfaces and extruded those again, using "cut" to create tubes through them.


![step_1](../images/week06/Screenshot_1.jpg)

### Combine

Then I moved the hollow cylinders, marking them and clicking "m" and subsequently clicking move type "point to opint".
To make this new body, that consisted that way one body, I marked it and clicked combine in the upper command bar.

![step_2](../images/week06/Screenshot_2.jpg)

### Fillet

To thicken the connection between the cylinders-tubes and the base block, I used the command "fillet" and filled out the narrow connections with material:

![step_3](../images/week06/Screenshot_3.jpg)

Then I sketched a circel into the canal in the cylinder, giving it a 0.4mm distance from the tubes end.This was recommended on the web: [Print movable components in one job](https://www.javelin-tech.com/blog/2018/05/3d-print-moving-components/).

![step_4](../images/week06/Screenshot_4.jpg)

This circle I then extruded to both side, through the hole and towards the other side through the other hole.

![step_5](../images/week06/Screenshot_5.jpg)

![step_6](../images/week06/Screenshot_6.jpg)

I made sure the rod would stick out on both sides by two centieters and then made two disks on the respective ends, ensuring the rod won't come out. Eventually I declared the resulting construct as one body:

![step_7](../images/week06/Screenshot_7.jpg)

To make sure that the distance between the rod and the inner wall of the cylinder tube/lug is not to narrow large, I used inspect/measure to make sure of that:

![step_8](../images/week06/Screenshot_8.jpg)

### Create new plane and move/joint operation

To connect the counter part to it, I created two cubes on the counter block, using sketched lines to measure and make sure that the distance to the outside of the rectangle is correct. From those I extruded two cylinders to connect them to the rod.
For this to work I needed to construct plane (construct &rarr; plane) as a reference point for the rods to connect to. That is because it is difficult to define a point to join to on a rounded surface.

![step_9](../images/week06/Screenshot_9.jpg)

When connecting the pieces, it is important to choose the operation/joint. Otherwise it is likely that operation cut is predefined. And that is not what I want here:

![step_10](../images/week06/Screenshot_10.jpg)

I realized that my connecting rods could be thicker, when merching with the rotating rod, so I decided to "offset face", clicking "q" and enlarging the diameter off the cylinders:

![step_11](../images/week06/Screenshot_11.jpg)

As a last step, I made holes into the base blocks to be able to later on connect them to something using nuts and bolts.

![step_12](../images/week06/Screenshot_12.jpg)

Then it was time to export the file as a stl file to get it ready for the 3D printer.

![step_13](../images/week06/Screenshot_13.jpg)

## Printing process

Then I opened it with cura, a software for ultimaker 3D printers. Cura gives a recommeded set of settings for the print, which can also be customised. Depending on how detailed a print is one can for example choose a certain layer height for the print outcome. (Range here: 0.06-0.4mm)

![step_14](../images/week06/Screenshot_14.jpg)

Since the part is rather small, I chose a layer height of 0.06mm. For the infill (density of plastic on the inside), I went with the recommendation of 20% to save some material.
It is in this case crucial, that support structures are enabled, especially since the part where the turning rod and the lugs are printed, it is needed , since otherwise the hot extruded plastics would melt together. Also of course parts that are printed above surface need support material, since they would otherwise melt down to the bottom. These support materials can later on be removed via plyers for examples:

![step_15](../images/week06/Screenshot_15.jpg)

The raw print with all support materials in place (using the ultimaker 2+ here):
![step_16](../images/week06/step_15.jpg)

The support structure was quickly removed with a set of pliers. First I wanted to remove the material in the lugs with a needle, but a lab supervisor suggested, that I could try holding the thickened end disks on the movable rod with the pliers and then try to move them. It worked breaking out the support material and the piece was ready to use:
![step_16](../images/week06/step_16.jpg)

The movable hinge:

![step_17](../images/week06/step_17.gif)

Download the 3D-print-file [here](https://gitlab.com/LukasKhl/fundamentals-project-2021/-/raw/main/docs/projects/3D_print/Hinge_v3.stl?inline=false).

Download the fusion design [here](https://gitlab.com/LukasKhl/fundamentals-project-2021/-/raw/main/docs/projects/3D_print/Hinge_v5.f3d?inline=false)

