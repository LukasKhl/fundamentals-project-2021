# 7. Electronics design - Making a PCB accustomed to my needs

This week the task was to design an own PCB (or modify an existing one). I definately need a board with several analog and some digital pins for my final project, so I decided to build a bridge between my final project and this week's task. 

## Choosing a board

The first thing I had to think about was which devices I want to use within my project and what I want to achieve. Since my project is about growing algae and decreasing fertilizer content, I knew I had to choose some sensors, that would show me if the conditions in the water are good for algae growth. For this purpose I chose a pH and a temperature sensor. Each alagae species has a preferred range of pH, in which it likes to grow and a preferred range of temperatures. 

Then I need a sensor, which shows me how much light penetrates the water. This is because the amount of light that can be perceived on the side of the waterfilled container, that is opposite to the light source tells me how dense the algae culture in the water has become or in other words how much algae is there to be harvested. For this purpose I want to either use a photoresistor or a phototransistor. As of now it does not matter what I use, since both need three pins on the board (VCC, GND and an analog one).

Then of course I need a sensor, that indicates how much fertilizer is dissolved in the waterbody. There are two ways of which I know how to do it: EC (electrical conductivity(V)) or TDS (total dissolved solutes(ppm)). After some research I figured, that an EC sensor is far more expensive than a TDS sensor. EC is actually the more common way to determine leftover fertilizer (at least in the region I come from), but since the price was 8x as high, I chose the other one.

Eventually I want to see the data that I collect and perhaps use it for a study. So I chose two devices to give me these outputs. One is an LCD display, which I use together with an LCD driver to decrease the amount of pins I need on the board for it. The other one is a bluetooth transceiver, which you can use for in-/output (in this case for output), because I want to send the data to my phone and be reminded, that I for example need to feed the alagae or harvest them.

List of devices:

 * VBESTLIFE 5 V PH-Sensormodul + PH-Sonde 
 * EXP-R15-019 Adafruit (temperature)
 * photoresistor/transistor (not yet defined)
 * CQRobot Ocean: TDS (Total Dissolved Solids) Meter Sensor
 * LCD 1602 Module + driver 
 * Bluetooth Wireless RF-Transceiver-Modul RS232 serielle TTL

After discussing the sensors I went to discuss with my supervisor which board to use. He adviced me to use one of his open source schematics called satshakit-mega based on a Mega eagle based on an Arduino Mega. He had the file uploaded in different versions for different production pathways. Since I wanted to use the cnc machine to make the board I used the version sathshakit_cnc, filetype KiCAD Schematic.
I downloaded the entire zip folder he provided and unpacked it:

![step_1](../images/week07/Screenshot_1.png)

## Preparing the board

### Schematics with KiCAD

I opened the file with keycad and started adding the pins for my sensors to the premade sketch. For that I clicked the add symbol sign on the right side on the KiCAD interface:

![step_2](../images/week07/Screenshot_2.png)

Then I move the cursor (the big cross) onto the spot where I want the component to be:

![step_3](../images/week07/Screenshot_3.png)

For most devices I need three pins (VCC, GND and the analog data connection). After clicking to drop the component, a list pops up, where a component can be chosen from a long list. Best is if you know what you are looking for, then type some letters in and you will be given choices to pick from. If you know you want a certain component from a certain library, minimize the ones you don't want, then you will find your desired component faster. In my case I want a generic connector, so I minimize the library connector and am straight away at "Connector_Generic" without scrolling all too much. To know if a device needs 2, 3 or 4 pins, I looked up the requirements of each of the devices online (and if they need a analog or digital connection).

![step_4](../images/week07/Screenshot_4.png)

Now you could hook up all the connectors to the respective digital or analog output of the microcontroller using the green lines, also indicated on the right toolbar in KiCAD:

![step_5](../images/week07/Screenshot_5.png)

Or you add a tag (the A in the red lined tag symbol):

![step_6](../images/week07/Screenshot_6.png)

This leads to a much more easy to overview sketch, since it is not crowded with overlapping green lines crossing each other.
![step_7](../images/week07/Screenshot_7.png)

Also you can give names to your connectors, so it is easier later to track which one belongs to which device. You do that by right clicking and then Properties/Edit Properties:

![step_8](../images/week07/Screenshot_8.png)

Change the Value for reference:
![step_9](../images/week07/Screenshot_9.png)

For the bluetooth transceiver and the LCD driver I needed 4 pins:
![step_10](../images/week07/Screenshot_10.png)

It is hooked to the SDA pin (transmits data) and the SCL pin (synchronizes the data transmissions). Here all the connectors so far and their corresponding pins on the microcontroller (ATMega) can be seen:
![step_11](../images/week07/Screenshot_11.png)

For the bluetooth transceiver digital pins are needed (RXD and TXD, R= receive data, T= transmit data):

![step_12](../images/week07/Screenshot_12.png)

This is the intermediary status of the board after pins for all my devices were declared:
![step_13](../images/week07/Screenshot_13.png)

One of the supervisors asked me then: "Why would you keep all these PADS/connectors that are declared for the digital pins?"
So I deleted them and just created a bunch of extra 3 pin connectors to have more potential connections for more digital devices:
![step_14](../images/week07/Screenshot_14.png)

I aslo wanted to simplify the switch:

![step_15](../images/week07/Screenshot_15.png)

And then declared all the digital 3 pin connectors:
![step_16](../images/week07/Screenshot_16.png)
The switch was equipped with a 2 pin connector for VCC and GND:
![step_17](../images/week07/Screenshot_17.png)

Then it was time to define actual physical devices used as the digitally declared connectors. This is done by generating a netlist.

### Generating a netlist - KiCAD

![step_18](../images/week07/Screenshot_18.png)

I first used the fab library (which I regretted later since I used an eagle file, which had me change everything later according to the eagle library):

![step_19](../images/week07/Screenshot_19.png)

Undiscerning I chose the 1mm diameter pinheaders.
![step_20](../images/week07/Screenshot_20.png)

Then I clicked apply, save schematic and continue and subsequently OK:
![step_21](../images/week07/Screenshot_21.png)

After the schematics, which define all the logic connections were finished, it was time to design the actual layout of the circuit board:
![step_22](../images/week07/Screenshot_22.png)

When the respective tab opened, I had to click update PCB from schematic:
![step_23](../images/week07/Screenshot_23.png)

Those were the sttings I chose for the update after being requested by the software to fill them out:

![step_24](../images/week07/Screenshot_24.png)

Then I realised, that this wouldn't include my changes that I made to it, so I enabled all the options to get my desired results:
![step_25](../images/week07/Screenshot_25.png)

### Creating the board layout with KiCAD

What happens next, is that all the devices I threw into the schematic appear in a large pile, that for reasons of better overview, I had to pick apart piece by piece. Then I started arranging the pieces (the connectors). I tried to bring them close to the data pins that they were supposed to be connected to. For the GND and the VCC it is mainly important that they are all respectively connected to each other and the circle is closed. 
****
**This part of the job is actually quite hard. It is all about logically connecting the traces without having them interfere with eachother. Do not despair if it takes you a couple hours if you do it the first time.**
****
Tip: The cursor that you use in this step of the process always jumps to the grid, so if you want to make finer or rougher steps, you can change the distance of the grid dots:

![step_26](../images/week07/Screenshot_26.png)

Here you see, how I connected the digital pins to the microcontroller (I actually rearranged the connectors in this case, going back to the schematics and updating the netlist, making pure data pin connectors and pure VCC and GND connectors, to be able to simplify drawing the traces and not deadlocking my work):
![step_27](../images/week07/Screenshot_27.png)

Eventually my board looked like this (forgot to hook up two capacitors to the microcontroller...ups!)
![step_28](../images/week07/Screenshot_28.png)

As you can see, I moved the bluetooth transceiver to the middle left. The other devices I picked are on the right side with GND and VCC above eachother respectively, to simplify their connections.

One of the supervisors then told me, that I had to use the connectors from the eagle library, not the fab library and use 1.4mm pins, for more mechanical stability.

After all he also told me to enlarge my trace size. At least I found a good way to do that fast via right_click/select/filter selection:

![step_29](../images/week07/Screenshot_29.png)
![step_30](../images/week07/Screenshot_30.png)

![step_31](../images/week07/Screenshot_31.png)

After finally being done, it was time to create a file, that would help me collect the parts I needed to solder later:

![step_32](../images/week07/Screenshot_32.png)

Leading to this excel file:
![step_33](../images/week07/Screenshot_33.png)

To indicate that the gaps between the traces shall not be all milled out, I filled them, clicking on the following item on the right:

![step_34](../images/week07/Screenshot_34.png)

And made sure to lable it as the ground (GND):
![step_35](../images/week07/Screenshot_35.png)

To indicate the zone to fill in, hold control to draw straight, perpendicular lines around the layout.
![step_36](../images/week07/Screenshot_36.png)

Resulting in:
![step_37](../images/week07/Screenshot_37.png)

### Export as SVG for inkscape

To work on it in inkscape, I then exported it as an SVG.

![step_38](../images/week07/Screenshot_38.png)

With following settings:

![step_39](../images/week07/Screenshot_39.png)

Then I opened the file with inkscape (somehow my computer saved it as in to open with microsoft edge...):

![step_40](../images/week07/Screenshot_40.png)

### Export from inkscape for Gimp as a png

From there I exported it as an png to be able to open it in GIMP:

![step_41](../images/week07/Screenshot_41.png)

![step_42](../images/week07/Screenshot_42.png)

I opened the file in Gimp and selected the areas with a range of different selection tools (Tools/selction tools/..) the rectangle selection tool:

![step_43](../images/week07/Screenshot_43.png)

![step_44](../images/week07/Screenshot_44.png)

Through Select/Rounded Rectangle... I also rounded the edges of the rectangle:

![step_45](../images/week07/Screenshot_45.png)

![step_46](../images/week07/Screenshot_46.png)

![step_47](../images/week07/Screenshot_47.png)

![step_48](../images/week07/Screenshot_48.png)

Since the white areas are the non-milled areas, I selected by color and inverted foreground and background.

![step_49](../images/week07/Screenshot_49.png)

## CNC milling the PCB

For the physical making of the PCB, I mainly used the steps found in week4 in PCB manufacturing, when making the traces though, I used a 0.2mm wide milling bit, since the traces are even closer to eachother. 

**The cut depth is on 0mm, because when setting the z-coordinate we make sure to penetrate the copper plate, which is already sufficient for removing the copper layer.**

![step_50](../images/week07/Screenshot_50.png)

With this "holder" I was able to fixate the small milling bit:

![step_51](../images/week07/Screenshot_51.jpg)

The 2mm diameter milling bit for the traces:

![step_52](../images/week07/Screenshot_52.jpg)

The 8mm milling bit for the outline and holes:

![step_53](../images/week07/Screenshot_53.jpg)

Since I fixated the Copper board in the mill with double sided tape, one side was too high resulting in an non completed depth of the PCB on the bottom right:

![step_54](../images/week07/Screenshot_54.jpg)

I therefore reopened the file in Gimp and cut out the bit needed again for fixing:

![step_55](../images/week07/Screenshot_55.jpg)
![step_56](../images/week07/Screenshot_56.jpg)
After the PCB was ready I polished it with acetone (also to make it possible to solder on it. Many times the copper has a protective layer, which makes it impossible to solder on it though).
![step_57](../images/week07/Screenshot_57.jpg)
Resulting in:
![step_58](../images/week07/Screenshot_58.jpg)

With the help of the previously created excel file, I then collected all the bits, that needed to be soldered onto the PCB:
![step_59](../images/week07/Screenshot_59.jpg)
![step_60](../images/week07/Screenshot_60.jpg)

Next thing to do was to solder all those devices onto the board (not forgetting to let the solder device heat up to at least 350°C and switching on the fume hood of course).
Small parts tin-solder/solder, larger parts that stick out like the pins need more for mechanical stability. My personal experience is that I need to hold the soldering iron close to but not touching the board and then to melt the solder onto the board and the respective end of the part to be soldered. It can also help to heat up the bit of the board and part to be soldered before actually applying the solder.

![step_61](../images/week07/screenshot_61.png)

After I was done soldering, I noticed quite some dirt left on the PCB. That I removed with isopropyl alcohol by directly spraying it on the dirty spots.

![step_62](../images/week07/screenshot_61_5.png)

Next I needed to check, if all the connections were soldered correctly and I did not accidentally connect two traces or somehow in the milling process destroy a trace.
The settings I put the voltmeter on, the device needed to do the check, were a direct current, that makes the voltmeter, if electrons are flowing, release a beeping sound. The red cable here indicates the VCC, the black one the GND (for that they need to be plugged to the voltmeter accordingly, but this color allocation is apparently the standard).

![step_62](../images/week07/screenshot_62.png)

To check if the traces to the single pins are working, I needed to hold one end to one of the pinouts and the other one to the connected pin.

![step_63](../images/week07/screenshot_63.png)

To check if the GND was all connected, I chose a pinout responsible for the GND on the chip and one far away on the board. Vice versa for VCC.

Checking the GND connection:
![step_64](../images/week07/screenshot_64.png)

During the check I realised, that one of the LEDs was broken and possibly also soldered the wrong way around. Since it didn't work no matter how I applied the voltmeter to it, I couldn't tell.
Anyways: Light emitting diodes only allow current in one direction. So the LED needs to be mounted the correct way. In the layout, the SCK side, the side connected to the microcontroller, is the side that is supposed to be connected to VCC, the one labelled with two is the one supposed to be connected to GND.

![step_65](../images/week07/screenshot_65.png)

To push down the plastic holdings on the pins, I used a pair of tweezers, grabbing the pins slightly and horizontally pushing down on the plastics.

![step_66](../images/week07/screenshot_66.png)

After I was done with the board, I wanted to make myself a sketch of how to connect the PCB to a programming board (arduino uno here). I connected Miso to Miso, Mosi to Mosi, SCK to SCK, VCC to 5V (VCC), GND to GND and doing so I just thought: Well then it must be reset to reset (RST) as well. But... it's not. One of the supervisors in our lab told me, that the reset is basically there to accept new orders from another device (very much simplified). So you wouldn't connect two same pinouts that both await orders with eachother. (Indicated here by the blue outcrossings)

![step_67](../images/week07/screenshot_67.png)

### Programming the PCBs embedded system

Before you can program the PCB, with the arduino, the arduino needs to be enabled to:

![step_67_5](../images/week07/Screenshot_76.png)

In the next picture you can read it: "Pin 10 is used to reset the target microcontroller".
This is the ISP sketch uploaded to the arduino first, before hooking it to my PCB. This makes the arduino a programm transmitter to my PCB so to say. (Also the connections of the other pins are described here.)

![step_68](../images/week07/Screenshot_70.png)

After that I made the connections between the arduino and my board as descrobed in the ISP sketch and then set the tool settings as seen (they can stay the same because the atmega is in its core the same as an arduino uno).
Make sure to choose the right port again, when hooking up the arduino+board:

![step_68_5](../images/week07/Screenshot_75.png)

Then I burned the bootloader as instructed:

![step_69](../images/week07/Screenshot_71.png)

When overtaking the code for the blink programm from the PCB manufacturing week, I needed to change the declaration of at which pin the LED is. On the schematic I found it to be at PB5.

![step_70](../images/week07/Screenshot_72.png)

PB5 turns out to be pin 13. So in the code, one can refer to it as 13.

```
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(13, OUTPUT);
}
```

![step_71](../images/week07/Screenshot_73.png)

Or to LED_BUILTIN, since there is just one.

```
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}
```
On the hook, the code is verified and on the arrow pointing right it is uploaded:

![step_74](../images/week07/Screenshot_74.png)

And the code + PCB proof to be working:

![step_75](../images/week07/screenshot_77.gif)

![step_76](../images/week07/Screenshot_78.jpeg)

When connecting the PCB to a FTDI cable, things work slightly different. The yellow and orange cable are connected to the TDX and RDX pins and the green cable to the reset pin specifically assigned for a FTDI cable. Red and black are connected to GND and VCC.

A colleague told me then that there are standardised colors for VCC, GND and RST:

* VCC: Red
* GND: Black
* RST: Green

The connector of the FTDI cable can be seen here:
![step_77](../images/week07/Screenshot_79.jpeg)

Download PCB schematics [here](https://gitlab.com/LukasKhl/fundamentals-project-2021/-/raw/main/docs/projects/Electronics_design_kicad/satshakit_cnc.sch?inline=false)

Download PCB layout [here](https://gitlab.com/LukasKhl/fundamentals-project-2021/-/raw/main/docs/projects/Electronics_design_kicad/satshakit_cnc.kicad_pcb?inline=false)



