# 1. Project management

This week I learned how to set up a website with git bash, git lab and Visual Studio Code. The purpose is to visualize the workflow and learning curve throughout this project and experience. Since I never did this before, I decided to edit its content with Markdown as it appeared to be the easiest and fastest option for a beginner.

## Markdown

First I learned how to fumble with markdown. Things I went through were for example learning how to write in bold letters, using (**) or italics (_). Then we learnt how to create headers with different sizes, using # or multiple #.
I also learned how to make lists, upload images and create links.
For details on how to do these things, check it out [here](https://www.markdowntutorial.com/lesson/1/).

## Git bash and git lab

To open git bash and work on the right directory (folder), I open it, right click and select "Git Bash here".

![gitbashhere](../images/week01/gitbashhere.png)

To be able to communicate the project content that I plan to create in the comming weeks to my git lab based website, I downloaded git bash and set up an account on git lab. On my computer I needed to create a folder for exactly that reason. This folder I had to then connect to git bash and git lab. To create a safe link between our computers and git lab, I needed to apply an SSH key, that helps to push and pull content from the computer to git bash and then to git lab, so I am eventually able to see the content on my website, as it can be seen now.
For tips on git bash one can use this [link](https://www.gnu.org/software/bash/manual/bash.html).

As of now I use the commands:
```
git add . or git add --all 
//adding the new changes from the folder and visual studio codes-->later more on that
git commit -m"..." 
//saving changes
git push 
//uploading them to git lab and the webpage
```


On git lab I can then have an overview of what I already uploaded, check back on previous statuses of the project and do a lot more things, that demand further research from my side, in order to figure out all the functions.

## Visual Studio code

The workshop's supervisor recommended us (the Windows users among us) to use Visual studio code to modify and update the content of our website. That is basically what I am doing right now...

Thankfully for the newbies among us for that we just need to open the folder of our fundamentals project and a lot was prepped already (folders, subfolders, textfiles, etc. &rarr; click openFolder and choose the respective one). Visual codes provides an explorer task bar on the left, making it easier for us to switch through the different tabs on our website as we need and finding images and other files we need to use and display.
To edit the content I use what I learned in the markdown tutorial and eventually I use git bash to push the changes to git lab and the website.
That is it for now. 

### Pictures added
In order to add a picture on the website I needed to use ![], in which I describe the name/title of a picture and () to show the path of the directories, where the picture is located so to say. This could look something like this for example:
```
![picture1](../.../.../images/picure1.jpg)
// or .../picture1.png)
```
Since there are data capacities of 1GB in total at our exposal for this website, it is also really important to lower the quality of our picture material, that I upload. To do so, I use the software gimp combined with a plugin called bimp.
With the help of gimp one can then decrease the numbers of pixels used or the quality of the picture in general:


![gimp](../images/week01/gimp.jpg)





