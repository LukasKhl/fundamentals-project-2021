# 2. Computer aided design - CAD

This week I was taught how to 2D Design with LibreCAD and 3D design with Fusion360. Since I never used any of these tools, it was tough, but with tips of colleagues and YouTube videos I managed to get something done.

## LibreCAD

Eventually I would like to make a box as a "safe house" for my microelectronics when placing them next to my algae tank.
In order to have a plan on how to do that, I drew a bottom piece with finger joints, where I indicated where which devices might be placed. For that I measured the length and with of an arduino uno and a bread board for example.

Download the file [here](https://gitlab.com/LukasKhl/fundamentals-project-2021/-/raw/main/docs/projects/CAD/3D_box_project_v5.f3d).

### My process with LibreCAD step by step

First I drew a rectangle as a base and then one being the breadboard.
![step_1](../images/week02/Libre/1_making_a_rectangle.png)

For that I defined the point of origin of each figure and a final point (upper right). To do that I used the command box on the bottom right.

![step_2](../images/week02/Libre/2_showing_where_the_rectangle_should_be_allocated.png)

To create more spots where I want to assign space to electronics parts I used the move/copy command:

![step_3](../images/week02/Libre/3_moving_and_copying_a_figure.png)

Since the next spot created was meant for a smaller device, I halfed the scale, which can be done through the scaling options, when moving and copying a figure.

![step_4](../images/week02/Libre/4_changing_the_dimensions_of_the_figure.png)

Then I added descriptions to the parts:

![step_5](../images/week02/Libre/5_writing_text.png)

With the intention to create extentions to the drawing (that could later be used in a box to stack pieces together) I snapped to the middle of the upper end and used it as a new point of origin to draw the extention from:

![step_6](../images/week02/Libre/6_creating_an_extension_by_moving_the_point_of_origin.png)

![step_7](../images/week02/Libre/7_creating_an_extension_by_moving_the_point_of_origin.png)

Then I used the divide function to remove unwanted line connections:


![step_8](../images/week02/Libre/8_using_divide_to_remove_a_line.png)

![step_9](../images/week02/Libre/9_line_removed.png)

Quickly I realized, that not all lines could be removed if we work with interrelated rectangle lines. Therefore I did redo the entire extension with simple lines:

![step_10](../images/week02/Libre/10_rectangle_could_not_be_divided_forced_me_to_replace_it_by_lines.png)

And the problem with the non-removable lines was resolved :-)
After that I did not want to go through each step of the creation of the extentions again, so I used move and copy and rotate to bring the same extension to the lower side (important is to decide in each case "keep original" or "delete" &rarr; when copying you most likely want to keep it, when rotating mostly you want to delete it).

![step_11](../images/week02/Libre/11_using_move_and_copy.png)

![step_12](../images/week02/Libre/12_cop_again_rotate_delete_original.png)

At that point the figure had "finger"extentions on top and bottom, but not on the sides. So I decided to scale the existing ones and find out the sizes in order to make them all the same.

![step_13](../images/week02/Libre/13_scaling_to_get_same_height.png)

![step_14](../images/week02/Libre/14_apllying_scale.png)

### Alternative

After creating my model I talked about it to a friend, who happened to know a website with which one could simply create an entire box by applying the wanted parameter:

![step_15](../images/week02/Libre/15_alternative_box_creation.png)

Additionally it offers to address the level of precision of the laser cutter, that one might use for cutting by giving the option to define a kerf:

![step_16](../images/week02/Libre/16_choose_a_kerf.png)

Eventually to be able to use it, one needs to extract the file as a dxf file.

![step_17](../images/week02/Libre/17_extract_as_dxf.png)

## Fusion 360

My aim with fusion is for now to create a case for the electronic parts that I will need for my project. Furthermore I plan to use a transparent material in the lid of the case to be able to see the electronics when the case is closed. That is mainly for visuals though.

Download the file [here](https://gitlab.com/LukasKhl/fundamentals-project-2021/-/raw/main/docs/projects/CAD/Bottom_of_microcontrollerbox.dxf).

### My process with Fusion360 step by step

First I drew a sketch of all the parts of my casing, that I would later assemble. For that I mainly needed the rectangle function, which can be found in the sketch menue. For the dimensions I used the values, which I already measured for the LibreCAD project.

![step_f1](../images/week02/Fusion360/0_drawing_sketch.png)

As can be seen: I constrained the upper rectangle, which posseses an inner rectangle in such way, that the lines of the inner and outer rectangle have the same distance from each other. The inner rectangle also posseses curved edges, which I created via the fillet function.

After being done with my sketch, I clicked finish sketch and started with the extrusion (10mm).

![step_f2](../images/week02/Fusion360/1_Extrude.png)

After that was done I needed to join the single pieces, that I created. Under assemble I found the join funciton. Then I had to click the respective positions on which the pieces where to be joined together.

![step_f3](../images/week02/Fusion360/2_joining_pieces.png)

![step_f4](../images/week02/Fusion360/3_choosing_attachement_point.png)

![step_f5](../images/week02/Fusion360/5_piece-by_piece.png)

![step_f6](../images/week02/Fusion360/6_piece_by_piece.png)

![step_f7](../images/week02/Fusion360/7_piece_by_piece.png)

![step_f8](../images/week02/Fusion360/8_piece_by_piece.png)

For the glaswindow, I started another sketch. I chose the plane of the upper side of the lid to do it, extruded it and put it exactly into the spot where the hole was. For appearance I chose the glas looking option (right click&rarr;appearance&rarr;...).

![step_f9](../images/week02/Fusion360/9_changing_material.png)

![step_f10](../images/week02/Fusion360/10_glas_window.png)

