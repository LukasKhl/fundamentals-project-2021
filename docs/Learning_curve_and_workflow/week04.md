# 4. Electronics production - PCB manufacturing

This week I learned how to prepare a PCB board via KiCAD, inkscape and GIMP.
It has to be mentioned, that I worked with a premade file for a PCB, that everyone in the lab has got access to. The design of a PCB will take place in a future session. This session is meant to teach us the physical part of the manufacturing, speaking of the CNC milling and soldering. So we just needed to donwload the file and prep it a little bit, to have it ready for milling.

## Preparing the file

To get started with the process, I first had to open the downloaded file with KiCAD.

![step_0](../images/week04/step_0.JPG)
![step_1](../images/week04/step_1.JPG)

After that, the file had to be exported as an SVG with the settings the lab instructor provided:

![step_2](../images/week04/step_02.JPG)

Then I needed to open the exported file in inkscape. Eventhough no folder for the directory had been chosen, the file was saved in the folder where the original file was opened in KiCAD with.

![step_3](../images/week04/step_03.png)

From inkscape the file had to be exported as a PNG so later on it could be opened in GIMP.

In GIMP I had to export three images. To do that, I first had to open the previously in inkscape converted file.

The borders left not much room for error, so they needed to be widened (2mm in this case) at first, using image/canvas_size.

![step_4](../images/week04/step_04.png)

To simplify changing all layers at once, the canvas size had to be applied to the entire image. Done under Layer/layer_to_imagesize. &rarr; so first image_canvas_size then Layer/Layer_to_imagesize.

Then to get the first needed image the background had to be recolored, using the bucketfill function, clicking on the background, switched the color to white. 

![step_5](../images/week04/step_05.png)

To create the traces though, black and white had to be inverted. The white parts are the bits spared by the milling process. So they are in our case the copper material left through which electricity can flow. You can pick invert in select, but here I needed to select it in colors. They are the ones to be inverted.

![step_6](../images/week04/step_06.png)

The now created image serves as the base for the traces and has to be exported.

![step_7](../images/week04/step_07.png)

Then I could go on with the next bit, which are the holes in the PCB. To do so, bucket fill the black background with white. Now only the holes are left and the image can be again exported.

![step_8](../images/week04/step_08.png)

Then to give the PCB an outlying frame, I drew a rectangle around it and round its corners.

![step_9](../images/week04/step_09.png)

This rectangle I clicked into and through edit I chose to fill it with the foreground color, which is currently set on white. Eventually I exported this file too as a png.

![step_10](../images/week04/step_10.png)

To get all three images ready for printing, it is necessary to work on them, which can be done via [fabmodules.org](http://fabmodules.org/).

![step_11](../images/week04/step_11.png)

Starting with traces, we need to open the image (png).

![step_12](../images/week04/step_12.png)

For the output format (new tab that pops up after opening the image), we choose the roland mill, which is the one we use for milling out the piece.

![step_13](../images/week04/step_13.png)

Then we get to the process tab, where we choose 
"traces 1/64", which indicates that we are using a 0.4mm milling bit, since we want to make sure, not to take off too much material.

![step_14](../images/week04/step_14.png)
![step_15](../images/week04/step_15.png)

For the output settings, the following ones are used:

![step_16](../images/week04/step_16.JPG)

(the specific roland mill is the MDX-40)

The process settings are the following:

![step_17](../images/week04/step_17.JPG)

When all is set, one needs to press calculate and subsequently save, to save the file as one that the roland machine can make use of.
After the traces are done, the outline is next.

![step_18](../images/week04/step_18.png)
Here we can work with a broader mill bit (1/32).

![step_19](../images/week04/step_19.png)

We use the same settings for output, but different ones for the process:
![step_20](../images/week04/step_20.PNG)

Finally we come to the point, where we need to drill the holes:
![step_21](../images/week04/step_21.png)

The settings for the output and process are the same as for the output file. Don't forget to calculate and save each file, then they are ready for milling.

## Milling
Via USB, I transferred them to the computer hooked up to the roland mill.

First the mill was to be prepared. For that I needed to position a gel pad with vacuum suction ability into the mill, using screws and an allen key.

![step_22](../images/week04/step_22.jpg)
![step_23](../images/week04/step_23.jpg)

The choice is between a suction pad ("large") on the left and ("small") on the right:
![step_24](../images/week04/step_24.jpg)

Suction can be applied to one of the pads, but also to both simultaneously by linking them with each other. In the picture only the left pad is hooked up (left opening closed, middle connected to tube -> vaccuum), but the suction tube could be applied to the left link and the one in the middle and the right could be connected via a linking tube preventing air comming in and creating the vaccuum.

![step_25](../images/week04/step_25.jpg)

Then the pump needs to be hooked to the grid and it is also not a bad idea to hook the implemeted LEDs up as well in order to be able to follow the work steps better.

![step_26](../images/week04/step_26.jpg)

Then there are the different mill bits, one for the traces (the first picture) and one for the holes and the outline (the second one).
![step_27](../images/week04/step_27.jpg)
![step_28](../images/week04/step_28.jpg)

The mill bit needs to be applied to the machine:
![step_29](../images/week04/step_29.jpg)

I tightened it first by hand, then used two wrenches to tighten it sufficiently (not too hard though).
![step_30](../images/week04/step_30.jpg)
![step_31](../images/week04/step_31.jpg)

Then it comes to setting the spindle with the milling bit to the right position, to define its origin coordinates x,y,z.
There is an arrow set to use for this and determine the speed with which the spindle shall move. The step size needs to be decreased when getting close to the point of origin, which seem suitable, especially when moving towards the material on the z-axis.
Best is to try to set it at one of the corners when using a new piece of material in order to save as much material as possible for later.
Before setting the z axis we turn the spindle on at 15000rpm, max speed) and slowly move towards our material (here it is copper). When we see (or hear), that the material is slightly penetrated, we set the z-origin (simultaneously with the x,y origin, if we haven't done so already).
![step_32](../images/week04/step_32.jpg)
By the way, x,y and z can also be defined individually.

Next to click is cut.
![step_33](../images/week04/step_33.jpg)
And then delete the files that have previously been used for milling.
![step_34](../images/week04/step_34.jpg)
Add the desired file and click output to start the process:
![step_35](../images/week04/step_35.jpg)

First I did the traces, then the outlines and the wholes (not forgetting to change the mill bit before, of course.)

In the end the outcome looked like this:
![step_36](../images/week04/step_36.jpg)

## Soldering
The next step was soldering, for which I used tools llooking like this (I forgot to photograph them, therefore a free liscense photo from the web):
<a title="oomlout, CC BY-SA 2.0 &lt;https://creativecommons.org/licenses/by-sa/2.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Soldering_iron_and_accessories.jpg"><img width="512" alt="Soldering iron and accessories" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Soldering_iron_and_accessories.jpg/512px-Soldering_iron_and_accessories.jpg"></a>
[wikimwdia](https://upload.wikimedia.org/wikipedia/commons/2/2d/Soldering_iron_and_accessories.jpg)

The head of the solder tools needs to be at 350°C for soldering, the LED recommendably switched on (above, for better view), the magnifier used and the adjacent fume hood turned on to deviate toxic smells from one's nose. The picture here shows only the soldering tool, the station and the cleaning pad. There is also a solder wire (from tin) though, which is melted onto the board to solder components on it.
To see which components to solder where, the keycad file of the boards is very helpful:
![step_37](../images/week04/step_37.jpg)

The components are the following:
![step_38](../images/week04/step_38.jpg)

Here we can see the dimensions of a microcontroller unit (mcu) in comparison to the board:
![step_39](../images/week04/step_39.jpg)
...soldered on:
![step_40](../images/week04/step_40.jpg)
in comparison to human fingers:
![step_41](../images/week04/step_41.jpg)
In other words, it is quite tricky to solder something this small, when doing it for the first time. (At least I thought so...).

After all components had been soldered on, it looked like this:
![step_42](../images/week04/step_42.jpg)

## Programming the board via Arduino
To give the board with the LED a function, the following guide was povided by the lab instructor:
![step_43](../images/week04/step_43.jpg)

For hooking up the arduino uno with the PCB, we were given the following sketch:
![step_44](../images/week04/step_44.jpg)
Result:
![step_45](../images/week04/step45.jpg)
Before burning the bootloader we were instructed to tripple check if our parameter settings were correct:
![step_46](../images/week04/step_46.jpg)

After that we can write our code and in this case get the LED blinking (1s on, 1s off).

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(2, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(2, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(2, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```

This results in the following:

![step_46](../images/week04/step_47_SparkVideo.gif)
