# 5. CNC milling 

This week I learned how to convert a 3D-design from fusion to a file, that could be passed on to a CNC milling machine.
For my training I decided to design a simplified version of a microalgae cell.

## Design process

In the lab I was told that we have 16x10cm or 9x10cm blocks at our disposal with a depth of 8cm each. I decided for the second option, so the first thing I did in Fusion was to create a foundation of a 90x100mm rectangle, that I extruded by 80mm. On this foundation I then drew a double lined circle that is supposed to depict the epidermis of a microalgae cell. (In microalgae the cell wall is permeable, because unlike most terrestrial plants, it takes up nutrients through the cell wall like terrestrial plants do through the roots.)
![step_0](../images/week05/step_0.png)

To have the cell stand out I "negatively" extruded the area outside the cell by 20mm and the inner by 5mm to let the cell wall stand out.
![step_1](../images/week05/step_1.png)

Then I made a sketch choosing the inner part of the cell area as a surface. There I modelled the chloroplast, the part where photosynthesis happens (here the larger area) and the nucleus. Within the chloroplast I drew a circle to later symbolize that the Calvin cycle (the process where CO<sub>2</sub> and H<sub>2</sub>O are transformed to 0<sub>2</sub> and glucose with the help of an enzyme and light as a catalysator).

![step_2](../images/week05/step_2.png)

I also intended to show the Granum (stack of thylakoids), where light energy is taken up. For that I made a rectangle with rounded edges, which I then copied twice and moved onto each other to symbolize the stacked thylakoids.
![step_3](../images/week05/step_3.png)

To picture the stack effect I trimmed some of the lines (scissor symbol).
![step_4](../images/week05/step_4.png)

Then I chose text under create (Calvin cycle and Nucleus).
![step_5](../images/week05/step_5.png)

To illustrate the Calvin cycle more nicely, I created a rectangle, trimmed it, turned it, copied it and put it in the right positions.
![step_6](../images/week05/step_6.png)
![step_7](../images/week05/step_7.png)
![step_8](../images/week05/step_8.png)
![step_9](../images/week05/step_9.png)
(After the last picture I trimmed the lines again, that pierce the triangles/arrows.)

All my symbols where in place. Afterwards I negatively and positively extruded them.
![step_10](../images/week05/step_10.png)

To give the text part some design of its own, I created a sketch on the bottom of the letters, which I extruded. The letters of nucleus had thereby been "waveshaped".
![step_11](../images/week05/step_11.png)

The word Calvin cycle I gave a stair design via extrusion.
![step_12](../images/week05/step_12.png)

And yes, I colored the model green in the mean time, just because I thought it was fitting to an algae cell. For that I selected the body, right click, click appearance, then material, then color and metallic green. I had to grab the symbol for green and drop it on the body.
![step_13](../images/week05/step_13.png)

## Prepare for milling

To get to the CAM mode to prepare for milling, I clicked design and then manufacture.
![step_14](../images/week05/step_14.png)

To set up the milling process, I foremost defined the milling bits, that will be used in the process. Normally you would use a caliber to measure the bits that are going to be used. Here I just used approximated values, that can be changed/edited once I am in the lab and measure the accurate values.
Note that I changed the text, since I was told that there are no mill bits narrow enough available in the lab to fit the gaps between the previous letters. So I made abbrevaiations with larger letters.
![step_15](../images/week05/step_15.jpg)
On the plus sign, a new mill bit can be defined (the two existing once, I predefined).
![step_16](../images/week05/step_16.jpg)
First I define the larger bit for the rougher jobs. That one is mainly broader(/has a larger diameter). To define the mill bit, things like the number of flutes("cutting blades"), the length of the flute threads, the shoulder length (part of the bit that is sticking out from the holder), etc...
![step_17](../images/week05/step_17.jpg)
In cutting data I defined the spindle speed to 9000 rounds per minute (rpm). I was recommended this value, since the heat created due to friction of the spinning mill bit is then still low enough, so that the wax material is most likely not going to melt.
![step_18](../images/week05/step_18.jpg)
For the more fine tune jobs, I define a mill bit with less diameter:
![step_19](../images/week05/step_19.jpg)
Again 9000rpm:
![step_20](../images/week05/step_20.jpg)
After the mill bits are defined, the milling process/pathway parameters and specifications need to be defined. Here Setup/newSetup needs to be clicked.
![step_21](../images/week05/step_21.jpg)
We set the xyz origin to the bottom left (the starting point of the milling process):
![step_22](../images/week05/step_22.jpg)
Fusion does not automatically know which body to mill out therefore it needs to be indicated:
![step_23](../images/week05/step_23.jpg)
The body can be chosen with some offset (e.g. relative size box with +1mm offset) or as a fixed size box, which I did:
![step_24](../images/week05/step_24.jpg)
For the rough clearing-process, I selected the adaptive clearing process, which is meant to mill off the rough material (in a z-axis/vertical approach):
![step_25](../images/week05/step_25.jpg)
First the right mill bit needed to be chosen (the broader one):
![step_26](../images/week05/step_26.jpg)
Since we wouldn't use a coolant, because we are using wax as a milling material (not that much heat is created):

![step_27](../images/week05/step_27.jpg)

In the passes section many parameters for the process can be defined (and are predefined). But for the maximum roughing step down, I chose 2.5mm, to make for a smoother transitions in the diagional sections (next 2 pictures).

![step_28](../images/week05/step_28.jpg)

![step_29](../images/week05/step_29.jpg)

Since I left the cutting tolerance on 0.1mm to leave room for error (previous picture), I switched off stock to leave since it seems as doing the same thing twice:

![step_30](../images/week05/step_30.jpg)

Then press ok:

![step_31](../images/week05/step_31.jpg)

This calculates the pathway of the tool:

![step_32](../images/week05/step_32.jpg)

Right click on the adaptive section and choose simulation, to check the result and for possible errors:

![step_33](../images/week05/step_33.jpg)

![step_34](../images/week05/step_34.jpg)

In the next picture it can clearly be seen that the milling tool is too broad for some sections. This indicates, that the smaller bit is to be used here.

![step_35](../images/week05/step_35.jpg)

For the fine tune finishing, I chose the "parallel" pathway:

![step_36](../images/week05/step_36.jpg)

Here I needed to choose the more narrow mill bit:

![step_37](../images/week05/step_37.jpg)

Here it was important to choose which parts not to mill again, to save time (for example the really non detailed parts on the outside). For that matter I went to the tab geometry and enabled "avoid/touch surfaces" and subsequently chose which surfaces not to mill again:

![step_38](../images/week05/step_38.jpg)

![step_39](../images/week05/step_39.jpg)

We are going more fine tune here and that can quickly be seen, when looking at the tab passses. Tolerance automatically goes to 0.01mm.

![step_40](../images/week05/step_40.jpg)

To ensure smooth diagonal surfaces, the stepover is set to 0.3mm:

![step_41](../images/week05/step_41.jpg)

The simulation reveals that most of the previously unmilled sections are now milled (Except a small section in the ribosome section):

![step_42](../images/week05/step_42.jpg)

To export files that can be processed by the mill bit, I clicked Setup/Create NC program:

![step_43](../images/week05/step_43.jpg)

Here I picked the roland MDX-40 milling machine for the process, which is available in the FabLab KaLi.

![step_44](../images/week05/step_44.jpg)

I selected the desired pathways and clicked post. That enabled the process of starting the file creation.

![step_45](../images/week05/step_45.jpg)

## Milling process

Eventually I used a 1mm mill bit for the parallel/the fine cut and a 2mm one for the rough cut.
This also meant that I had to readjust the tool settings. For the 2mm tool, I also defined, that it shall stick out of the holder by 35mm, the 1mm stack out by 15mm.

After I applied the new settings, I had to right click the 2 pathways and click generate, so the toolpathways would actually be regenerated with the new settings. Then it is important to check the simulation again and make sure there are no collisions.

![step_45.5](../images/week05/step_45.5.jpg)

And here the two mill bits are to be seen:
![step_46](../images/week05/step_46.jpg)

To measure as in how far to stick them into the holder, so that 35 or 15mm respectively would stick out, I used a caliber. The caliber often has some presettings, in this case -0.02mm. To "zero"/reset it, it is necessary to click on ZERO.

![step_47](../images/week05/step_47.jpg)

![step_48](../images/week05/step_48.jpg)

It is possible to measure with the two grapples on the left, putting something in between or to use the two grapples on the right, expanding them and hitting the inside of the outer walls of something, you could measure its inner dimensions. Or you could use the little metal sheat that comes out at the bottom, when you move the lower grapple of the caliber downwards and measure the depth of something.

![step_49](../images/week05/step_49.jpg)

When working with something like 20mm, a value of 19.68mm is mostly fine to use, as the difference is very minor. It depends on how accurate your final product has to be.

![step_50](../images/week05/step_50.jpg)

When the mill is set up (as in the previous weeks project with the PCB manufacturing &rarr; check out for more details), I had to again use the arrows to define the X,Y value and the Z value for the origin of the cut. As defined in the design, this is the front left corner.

![step_51](../images/week05/step_51.jpg)

In this GIF, it can be seen, that this mill moves in 3 directions only. Downwards to "drill out" material and on the X and Y axis. Here one can see that it takes triangle alike shaped lines getting smaller and smaller, to move closer to the shape of a circle:

![step_52](../images/week05/step_52.gif)

I actually used a to high rpm value at first, making the material (a kind of sterofoam, not wax as I first assumed we would use) melt and stick to the mill bit in a natural bee hive shaped form.
Due to the tip of a co-worker, I soaked it in acetone to later remove it with pliers. It literally baked onto the mill bit... &rarr; Never use a too high rpm. I decreased from 11500rpm to 6000 with a new set of material and then I was fine.

![step_53](../images/week05/step_53.jpg)

For the next step I had to change the mill bit from the 2mm wide one, to the 1mm wide one.
It was then also necesary to set the Z origin again, since the 1mm wide mill bit was 10mm shorter.
There are two ways that can be done: 

1. Go to the original Z point and go down 10mm, then reapply the origin.

2. We removed 20mm of material from our foam on its corner. Go back down with the mill bit tip until you touch the corner, then go 20mm up and reapply the origin point and you have your new Z orgin according to your new mill bit length.

Here is the final result:

![step_54](../images/week05/step_54.jpg)
(I'd probably not make something as small as the arrows again. As you can see one broke off above the calvin cycle. )

Download rough pathway [here](https://gitlab.com/LukasKhl/fundamentals-project-2021/-/raw/ad4e586e92669598d19ead2ad62e72c19807eade/rough_4mm.prn?inline=false).

Download fine pathway [here](https://gitlab.com/LukasKhl/fundamentals-project-2021/-/raw/main/docs/projects/milling_files/fine_1mm.prn?inline=false).


Download the fusion design [here](https://gitlab.com/LukasKhl/fundamentals-project-2021/-/raw/main/docs/projects/milling_files/algae_cell_v7.f3d?inline=false)