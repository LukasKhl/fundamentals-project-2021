# 11. Final project

For the final project I want to combine what I already did within the last few weeks in the FabLab and a few new constructions and codes.

As explained throughout the course of the last couple weeks, I want to build a sensor station, that informs me about the status of my cultivated  algae culture (pH, temperature, total dissolved solutes and light intensity).

Of course the sensors shall not just loosely hang around, just as the PCB, that I made to control all data in- and output ([check the PCB manufacturing here](https://lukaskhl.gitlab.io/fundamentals-project-2021/assignments/week07/)). Therefore this section of my gitlab side shall deal with building the boxes/containers for the sensors and the PCB and last changes in the code loaded onto the PCB.

Therefore I seperate this chapter into a hardware and a software part.

## Hardware

### The hinge

For the PCB container I want to reuse the box I made in week 3 ([check my work process here](https://lukaskhl.gitlab.io/fundamentals-project-2021/assignments/week03/)). As can be seen there is an openeing on top. For this I plan to lasercut a plexiglas window connected to the box via a hinge.

![step_0](../images/week_11/Screenshot_0.jpg)

To make the hinge I first drew the outlines of the top of the box, in order to have a reference for how the outlines of the hinge shall be designed. Of course the lines needed to be transformed into construction lines.

![step_1](../images/week_11/Screenshot_1.png)

On the curb of the box I made another sketch drawing the bottom of the hinge, to later extrude it. 

![step_2](../images/week_11/Screenshot_2.png)

Onto the base of the hinge I drew some extensions to fit the rod of the hinge into it.

![step_3](../images/week_11/Screenshot_3.png)

![step_4](../images/week_11/Screenshot_4.png)

Into those extensions I started another sketch. This sketch consisted of a circel a little wider than the rod that I subsequently wante to create, so there would be space to move it and make it a functional hinge.

![step_5](../images/week_11/Screenshot_5.png)

After the sketch followed the negative extrude with operation "cut".

![step_6](../images/week_11/Screenshot_6.png)

![step_7](../images/week_11/Screenshot_7.png)

Inside the sketch of the circle I drew another circle that I extruded as a body. The distance to the inner wall created in the previous extrusion is 0.4mm, to leave room for movement. To the end of the rod I created extensions in order to create a barrier, so that it wouldn't slip out of its mount.

![step_11](../images/week_11/Screenshot_11.png)

![step_12](../images/week_11/Screenshot_12.png)

Then I copied the construct and created a midplane in the adjacent rod to make it possible to draw sketches from it and to then combinie the two through connecting links:

![step_inbetween](../images/week_11/Screenshot_8.png)

![step_inbetween](../images/week_11/Screenshot_9.png)

The final result looks roughly like this (the view is a little tilted since the angle looking at the construct is top/diagonal):

![step_12_1](../images/week_11/Screenshot_12_1.JPG)

Exported to Cura (3D PRINT SOFTWARE), I made sure there was support material in the joints of the hinge, which could be removed after the print as in my smaller version of this hint from [week 6](https://lukaskhl.gitlab.io/fundamentals-project-2021/assignments/week06/).

![step_12_2](../images/week_11/Screenshot_12_2.JPG)

Unfortunately the print came out with the rod and the joints melted together, so I copied the bits which included the joints, split them in the middle and added shields on their sides to glue them around the rod later on.

![step_12_3](../images/week_11/Screenshot_12_3.png)

The original print's joint carriers I removed via the proxxon tool ( a tool with the possibility to add tiny blades and grindstones). I then attached the new ones with the joints resulting in the following image:

![step_12_4](../images/week_11/Screenshot_12_4.png)

To actually give the box a see through lid, I made a quick 2D design of the lid and cut it out with the lasercutter.

![step_12_5_1](../images/week_11/Screenshot_12_5_1.JPG)

I also bought M3 threads to attach hinge, lid and the box.

![step_12_5](../images/week_11/Screenshot_12_5.jpg)

Just the screen is missing...

![step_12_inbetween](../images/week_11/Screenshot_12_9.JPG)

It took several tries to fit it on properly. The threads had to inserted into the plexiglas by using a hot soldering iron. I actually had to cut the lid a second time and make sure the insert holes for the threads are really on the right spot so everything would fit with no problems.

![step_12_6](../images/week_11/Screenshot_12_6.JPG)

![step_12_7](../images/week_11/Screenshot_12_7.JPG)

The hinge is also working:

![step_12_8](../images/week_11/Screenshot_12_8.JPG)

### The sensor container

Now I still needed a container for the sensors, so I 3D designed it to fit on top of the algae container galsses [to be seen here](https://lukaskhl.gitlab.io/fundamentals-project-2021/).

![step_13](../images/week_11/Screenshot_13.png)

![step_14](../images/week_11/Screenshot_14.png)

After the design in Fusion I printed the container with the Cura 2+ 3D printer.

![step_14_1](../images/week_11/Screenshot_14_1.JPG)

Because I was later on recommended to fit the sensors into threaded mounts designed for this purpose, I had to drill the holes a littel larger:

![step_14_2](../images/week_11/Screenshot_14_2.JPG)

![step_14_2_1](../images/week_11/Screenshot_14_2_1.JPG)

I also made a quick 2D design again for the lid and cut it with the laser cutter:

![step_14_3](../images/week_11/Screenshot_14_3.JPG)

### PCB layout for fast device attachement

For being able to attach and disattach the cables, sensors and other used devices quicker, I made a card labelling where to attach which devices and how.

![step_16](../images/week_11/Screenshot_16.png)

![step_16_1](../images/week_11/Screenshot_16_1.jpg)

### Phototransistor container

To have a container that shuts out light other than that coming through the glas, I created a small PCB, that gives a platform for the phototransistor, that it can stand on:

![step_17](../images/week_11/Screenshot_17.png)

![step_17_1](../images/week_11/Screenshot_17_1.png)

![step_17_2](../images/week_11/Screenshot_17_3.png)

After I made it with kiCAD and cut it out of a copper plate with a CNC mill, I soldered pins for the cables on it.

![step_17_3](../images/week_11/Screenshot_17_4.JPG)

![step_17_4](../images/week_11/Screenshot_17_5.JPG)

And I designed a container in Fusion360, which I then printed out with the Cura Ultimaker 3D printer.

![step_17_6](../images/week_11/Screenshot_17_6.JPG)

### Making my own cable elongations

To make sure cable length would be sufficient from the PCB to the sensor container, I made my own cables, using jumper wires, shrink tubes, crimp pins and cases.

![step_18](../images/week_11/Screenshot_18.JPG)

Applying the heat gun shrinks the tubes and let's them settle around the jumper wires.

![step_19](../images/week_11/Screenshot_19.JPG)

![step_20](../images/week_11/Screenshot_20.JPG)

### Making a new PCB

As can be seen, the cables connecting the sensors and the PCB come in through the back-wall of the box and are attached in a 90 degree angle. This unfortunately causes tension in the cables, which in combination with the short pins leads to the cables disttaching themselves. 

![step_21](../images/week_11/Screenshot_21.JPG)

For this reason I remade the PCB with 90 degree pins with longer pinheads.

![step_22](../images/week_11/Screenshot_22.JPG)

And I stuck the straight ones through the bottom in order to ensure that they are longer:

![step_23](../images/week_11/Screenshot_23.JPG)

Here is the comparison between the new and the old PCB:

![step_24](../images/week_11/Screenshot_24.JPG)

And because I previously attached a wrong phototransistor to my PCB/phototransistor mount, I had to take it off (it was a phototransistor for measuring infrared lights). In the process one of the desoldering one of the traces broke, so I needed to bridge it with solder applying the new Phototransistor.

![step_25](../images/week_11/Screenshot_25.JPG)

Here is the sensorstation in the algae cultivation water from two angles:

![step_27](../images/week_11/Screenshot_27.JPG)

![step_28](../images/week_11/Screenshot_28.JPG)

## Software

### Code update

The code status now is the following (see below). Added to it is the arduino json library. That is to export the measured parameters in array in json format. To transmit the data I use a bluetooth transceiver, that uses bluetooth 4.0. This version of bluetooth is compatible with more devices, than the version 2.0, that my previous transceiver was operating with.

```
#include <Wire.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
#include <SoftwareSerial.h> // bluetooth
#include <ArduinoJson.h>

#define ONE_WIRE_BUS 15 // Data wire is plugged into pin 15 on the Arduino, this is the TEMPERTURE sensor
#define TdsSensorPin 14 // TDS sensor is on pin 14
#define VREF 5.0      // analog reference voltage(Volt) of the ADC
#define SCOUNT  30           // sum of sample point
#define Phototransistor 17  // Phototransistor is on pin 17

int analogBuffer[SCOUNT];    // store the analog value in the array, read from ADC
int analogBufferTemp[SCOUNT];
int analogBufferIndex = 0, copyIndex = 0;

float averageVoltage = 0, tdsValue = 0, temperature = 25;

const int adcPin = A2;
// calculate your own m using ph_calibrate.ino
// When using the buffer solution of pH4 for calibration, m can be derived as:
// m = (pH7 - pH4) / (Vph7 - Vph4)

const float m = -5.436;

char c=' ';         // bluetooth
boolean NL = true;  // bluetooth

SoftwareSerial BTserial(11, 10); // RX, TX // bluetooth

LiquidCrystal_I2C lcd(0x27,20,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display


OneWire oneWire(ONE_WIRE_BUS); // Setup a OneWire instance to communicate with any OneWire devices


DallasTemperature sensors(&oneWire); // Pass OneWire reference to Dallas Temperature

StaticJsonDocument<200> doc;

void setup(void)
{
    Serial.begin(9600);
    sensors.begin(); // Start up the library
    pinMode(Phototransistor, INPUT);
    pinMode(TdsSensorPin, INPUT);
    lcd.init();                      // initialize the lcd 
    // Print a message to the LCD.
    lcd.backlight();
    lcd.setCursor(1,0);
    lcd.print("hello everyone");
    lcd.setCursor(1,1);
    lcd.print("konichiwaa");

    Serial.begin(9600);
    Serial.print("Sketch: "); Serial.println(__FILE__);
    Serial.print("Uploaded: "); Serial.println(__DATE__);
    Serial.println(" ");
    BTserial.begin(9600);
    Serial.println("BTserial started at 9600");
    Serial.println(" ");
    BTserial.println("BTserial started at 9600");

    
}

void loop(void)
{
   
    sensors.requestTemperatures();    
    // call sensors.requestTemperatures() to issue a global temperature
    // request to all devices on the bus
    // Send the command to get temperature readings

    Serial.println("Temperature is: " + String(sensors.getTempCByIndex(0)) + "°C");
    float Temp = sensors.getTempCByIndex(0);
    
    // You can have more than one DS18B20 on the same bus.
    // 0 refers to the first IC on the wire

  static unsigned long analogSampleTimepoint = millis();
  if (millis() - analogSampleTimepoint > 40U)  //every 40 milliseconds,read the analog value from the ADC
  {
    analogSampleTimepoint = millis();
    analogBuffer[analogBufferIndex] = analogRead(TdsSensorPin);    //read the analog value and store into the buffer
    analogBufferIndex++;
    if (analogBufferIndex == SCOUNT)
      analogBufferIndex = 0;
  }
  static unsigned long printTimepoint = millis();
  if (millis() - printTimepoint > 800U)
  {
    printTimepoint = millis();
    for (copyIndex = 0; copyIndex < SCOUNT; copyIndex++)
      analogBufferTemp[copyIndex] = analogBuffer[copyIndex];
    averageVoltage = getMedianNum(analogBufferTemp, SCOUNT) * (float)VREF / 1024.0; // read the analog value more stable by the median filtering algorithm, and convert to voltage value
    float compensationCoefficient = 1.0 + 0.02 * (temperature - 25.0); //temperature compensation formula: fFinalResult(25^C) = fFinalResult(current)/(1.0+0.02*(fTP-25.0));
    float compensationVolatge = averageVoltage / compensationCoefficient; //temperature compensation
    tdsValue = (133.42 * compensationVolatge * compensationVolatge * compensationVolatge - 255.86 * compensationVolatge * compensationVolatge + 857.39 * compensationVolatge) * 0.5; //convert voltage value to tds value
    //Serial.print("voltage:");
    //Serial.print(averageVoltage,2);
    //Serial.print("V   ");
    Serial.print("TDS----Value:");
    Serial.print(tdsValue, 0);
    Serial.println("ppm");
  }

  float Voltage= volt(17);
  Serial.print("Light/conductivity relation = ");
  Serial.print(Voltage);
  Serial.println(" Volt");

 

  float Po = analogRead(adcPin) * 5.0 / 1024;
  float phValue = 7 - (2.5 - Po) * m;
  Serial.print("ph value = "); 
  Serial.println(phValue);

  lcd.init();                      // initialize the lcd 

  // Print a message to the LCD.
  lcd.backlight();
  lcd.setCursor(1,0);
  lcd.print("Light: " + String(Voltage) + "V");
  lcd.setCursor(1,1);
  lcd.print("TDS:" + String(tdsValue, 0) + "ppm");  // ,0 --> 0 Stellen hinterm Komma
  delay(5000);
  lcd.clear();
  lcd.backlight();
  lcd.setCursor(1,0);
  lcd.print("pH: " + String(phValue));
  lcd.setCursor(1,1);
  lcd.print("Temp:" + String(sensors.getTempCByIndex(0)) + "degC");
  delay(5000);
  lcd.clear();


//  // Read from the Bluetooth module and send to the Arduino Serial Monitor // bluetooth
//  if (BTserial.available())
//  {
//  c = BTserial.read();
//  Serial.write(c);
//  }
//  // Read from the Serial Monitor and send to the Bluetooth module // bluetooth
//  if (Serial.available())
//  {
//  c = Serial.read();
//  BTserial.write(c);
//  // Echo the user input to the main window. The ">" character indicates the user entered text.
//  if (NL) { Serial.print(">"); NL = false; }
//  Serial.write(c);
//  if (c==10) { NL = true; }
//
//  } 
  doc["TDS"] = tdsValue;
  doc["Temperature"]   = Temp;
  doc["Lightstrength"] = float(Voltage);
  doc["pH"] = phValue;
  serializeJson(doc, Serial);
  serializeJson(doc, BTserial);
  BTserial.println();
}

float volt(int adPin)
{

return float(analogRead(adPin)) *5.0/1024.0;
  
}


int getMedianNum(int bArray[], int iFilterLen)
{
  int bTab[iFilterLen];
  for (byte i = 0; i < iFilterLen; i++)
    bTab[i] = bArray[i];
  int i, j, bTemp;
  for (j = 0; j < iFilterLen - 1; j++)
  {
    for (i = 0; i < iFilterLen - j - 1; i++)
    {
      if (bTab[i] > bTab[i + 1])
      {
        bTemp = bTab[i];
        bTab[i] = bTab[i + 1];
        bTab[i + 1] = bTemp;
      }
    }
  }
  if ((iFilterLen & 1) > 0)
    bTemp = bTab[(iFilterLen - 1) / 2];
  else
    bTemp = (bTab[iFilterLen / 2] + bTab[iFilterLen / 2 - 1]) / 2;
  return bTemp;

 // tdsValue, Temp,  volt, phValue
}

```

The variables for TDS (total dissolved solutes), pH, temperature and light intensity are turned into elements in an array /"serialized".

```
  doc["TDS"] = tdsValue;
  doc["Temperature"]   = Temp;
  doc["Lightstrength"] = float(Voltage);
  doc["pH"] = phValue;
  serializeJson(doc, Serial);
  serializeJson(doc, BTserial);
  BTserial.println();
}

```

In the json format they can later be transferred to other applications in order to eventually display them somewhere for example or make them subject of statistical anlysis.

serializeJson(doc, Serial); &rarr; transfers the json array into the serial monitor.
serializeJson(doc, BTserial); &rarr; transfers the json array into the bluetooth serial buffer.
serializeJson(doc, BTserial); &rarr; prints the array into the respective bluetooth terminal.

## Video

<iframe width="560" height="315" src="https://www.youtube.com/embed/GLziGh50Y2U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Sound by bensound

Thanks to Daniele Ingrassia, Ahmed Abdellatif, Harley Nelson and everyone that helped or gave advice.