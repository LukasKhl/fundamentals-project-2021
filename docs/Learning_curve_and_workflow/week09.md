# 9. Input devices

This week's focuss is on adding more input devices and hook them up to the selfmade PCB, create a code and use them with the device.
I kind of already did that last week with the temperature sensor, but will gladly do it again, since I designed my board to run with 4 sensors in total.

## Which sensor?

First of all: This time I use a TDS sensor. It measures the total dissolved of solutes (TDS) in a liquid and thereby gives a reference to how much fertilizer is dissolved in the waterbody. 
It can also serve to find out if water is contaminated:
![picture](../images/week09/Screenshot_0.png)

This is how it can help when determining the right fertilizer amount in plants when growing hydroponically (I made this Excel table after finding the values online):
![picture](../images/week09/Screenshot_0.5.png)

A very common way to determine fertilizer in water in this region is using EC (electrical conductivity). There is a rough correlation between EC and TDS. One could use a conversion factor of ~0.6. TDS is measured in ppm (partspermillion) and EC in S/m (Siemesn per meter).

&rarr; 
 * TDS=EC*(0.6)
 * EC=TDS/(0.6)

 I actually asked the manufacturer about this. Here is their answer: 
 ***
 "There is no mathematical relationship between TDS and EC values, we can only roughly estimate the level of conductivity from TDS. Because the conductivity of different ions is different, even under the same TDS, due to the different composition and structure of the water body, the EC value is also different."
 ***
The manufaturer provided information on how to hook up the sensor to an arduino and provided some source code:
![picture](../images/week09/Screenshot_1.png)
sample of the code:
![picture](../images/week09/Screenshot_2.png)
...

In my case, I figured I need to connect the data cable to the second pinout on the right of the atmega.

![picture](../images/week09/Screenshot_3.png)

That would be pin A0 or 14. Since I want to use the temperature sensor and this one together, I will use 14 and 15 (somehow they can both be referred to as A0 according to this pinout-sketch, but that would not work for me then, since they'd have the same name).
![picture](../images/week07/Screenshot_73.png)

This is what the code is unchanged, apart from A1 to 14:
```
#define TdsSensorPin 14
#define VREF 5.0      // analog reference voltage(Volt) of the ADC
#define SCOUNT  30           // sum of sample point
int analogBuffer[SCOUNT];    // store the analog value in the array, read from ADC
int analogBufferTemp[SCOUNT];
int analogBufferIndex = 0, copyIndex = 0;
float averageVoltage = 0, tdsValue = 0, temperature = 25;

void setup()
{
  Serial.begin(115200);
  pinMode(TdsSensorPin, INPUT);
}

void loop()
{
  static unsigned long analogSampleTimepoint = millis();
  if (millis() - analogSampleTimepoint > 40U)  //every 40 milliseconds,read the analog value from the ADC
  {
    analogSampleTimepoint = millis();
    analogBuffer[analogBufferIndex] = analogRead(TdsSensorPin);    //read the analog value and store into the buffer
    analogBufferIndex++;
    if (analogBufferIndex == SCOUNT)
      analogBufferIndex = 0;
  }
  static unsigned long printTimepoint = millis();
  if (millis() - printTimepoint > 800U)
  {
    printTimepoint = millis();
    for (copyIndex = 0; copyIndex < SCOUNT; copyIndex++)
      analogBufferTemp[copyIndex] = analogBuffer[copyIndex];
    averageVoltage = getMedianNum(analogBufferTemp, SCOUNT) * (float)VREF / 1024.0; // read the analog value more stable by the median filtering algorithm, and convert to voltage value
    float compensationCoefficient = 1.0 + 0.02 * (temperature - 25.0); //temperature compensation formula: fFinalResult(25^C) = fFinalResult(current)/(1.0+0.02*(fTP-25.0));
    float compensationVolatge = averageVoltage / compensationCoefficient; //temperature compensation
    tdsValue = (133.42 * compensationVolatge * compensationVolatge * compensationVolatge - 255.86 * compensationVolatge * compensationVolatge + 857.39 * compensationVolatge) * 0.5; //convert voltage value to tds value
    //Serial.print("voltage:");
    //Serial.print(averageVoltage,2);
    //Serial.print("V   ");
    Serial.print("TDS----Value:");
    Serial.print(tdsValue, 0);
    Serial.println("ppm");
  }
}
int getMedianNum(int bArray[], int iFilterLen)
{
  int bTab[iFilterLen];
  for (byte i = 0; i < iFilterLen; i++)
    bTab[i] = bArray[i];
  int i, j, bTemp;
  for (j = 0; j < iFilterLen - 1; j++)
  {
    for (i = 0; i < iFilterLen - j - 1; i++)
    {
      if (bTab[i] > bTab[i + 1])
      {
        bTemp = bTab[i];
        bTab[i] = bTab[i + 1];
        bTab[i + 1] = bTemp;
      }
    }
  }
  if ((iFilterLen & 1) > 0)
    bTemp = bTab[(iFilterLen - 1) / 2];
  else
    bTemp = (bTab[iFilterLen / 2] + bTab[iFilterLen / 2 - 1]) / 2;
  return bTemp;
}
```
For using less data, I went down to a baud rate of 9600:
![picture](../images/week09/Screenshot_5.png)

Checking the local tap water:
![picture](../images/week09/Screenshot_6.JPG)

Getting the output from the temperature and the TDS sensor:
![picture](../images/week09/Screenshot_7.JPG)

The whole setup working together:
![picture](../images/week09/Screenshot_8.gif)
