# Home - Algae Farm project

## Hello there!

![Algae farming enthusiast](./images/week01/Algae_Homefarm.jpg)

## Welcome to my project site

My aim in the FabLab is to learn about digital fabrication techniques in order to use them in my field of studies and work. That would be agriculture. My first project shall be about automizing one or two steps in algae farming.
Within this project I received a lot of help from the supervisor's of the workshop/lab "FabLab Kamp Lintfort". So a big thank you goes out to them!

