# About me

![Me](../images/Avatar/Avatarofmyself.png)

Hi! I am Lukas. I am a agricultural engineer and scientist based in NRW, Germany working on new biological fertilizers and greenhouse technologies. Occasionally I do some videography and editing, organize professional debates and presentations about agricultural innovations and write reports about newest developments in the sector and the region of the lower rhine.
Lately I have been quite fascinated with the opportunities in environmental data collection, that you get, when being able to install and set up sensors and microcrontrollers yourself. This is why I do the Fundamentals course in digital fabrication

## My background

I was born in the West of Germany close to the Netherlands. After highschool I was able to travel bits of every continent in the world doing all sorts of jobs. After that I studied Sustainable Agriculture close to the town of Kleve. 

## Previous work

I now work in a position that allows me to do some research and development and PR work at the same time.
Before and during my bachelor studies I did all kinds of things from latest being a lab assistent and a tutor going back to things like farm work, car washing and bar tending.

### Anything else?

When not involved in my work, I like road triping and exploring nature on my mountainbike... And any kind of outdoor activity really.
